#! /usr/bin/env bash

general_path="/mnt/scratch/users/bio_264_uma/luisdiaz/TYLCV_HI"
OUTPUT_FOLDER=$general_path"/analysed_samples"
SAMPLES_FILE=$general_path"/samples_to_process.lst"
RESULTS_FOLDER=$general_path"/final_results"
TARGET_FILE=$general_path"/target.txt"

#REFS='r1_M_Clon_R_Clon,r2_M_Clon_R_Clon'

virus_reference=$general_path"/virus.fasta"
raw_files=$general_path"/raw_reads"
consensus_start=1
consensus_end=2791
circular='-c'
rec_prob='0.001' #'0.000007'
orfs='155l116ldir;315l258ldir;164l359lrev;321l100lrev;1152l135lrev;1300l134lrev' #Coord base 1
group1="2698:2536:2332:1879:1813:1513:1512:1283:1280:1242:1046:1045"
group2="931:923:872:860:839:821:760:731:707:572:539:501:470:355:163:141:100:88:55:46"
min_nt_qual=26

#Como toques a partir de aqui, te cuelgo xD
OTHER_VARS="\$virus_reference=$virus_reference,\$consensus_start=$consensus_start,\$consensus_end=$consensus_end,\$circular=$circular,\$rec_prob=$rec_prob,\$orfs=$orfs,\$raw_files=$raw_files,\$min_quality_read=$min_nt_qual,\$group1=$group1,\$group2=$group2"

#CHECK SAMPLES
if [ "$1" == "0" ] ; then
	echo "Cheking workflows stage 1"
	./check_all_samples.sh $OUTPUT_FOLDER $SAMPLES_FILE "$2"
fi

#STAGE 1 (workflow)
if [ "$1" == "1" ] ; then
	echo "Launching stage 1"
	./launch_all_samples.sh $OUTPUT_FOLDER $SAMPLES_FILE "$OTHER_VARS" $2
fi

#STAGE 2 (Sample comparison)
if [ "$1" == "2" ] ; then
	echo "Launching stage 2"
	./compare_all_samples.sh $OUTPUT_FOLDER $RESULTS_FOLDER $TARGET_FILE $REFS
	#sbatch make_trees.sh $OUTPUT_FOLDER $RESULTS_FOLDER $TARGET_FILE $virus_reference 
fi
