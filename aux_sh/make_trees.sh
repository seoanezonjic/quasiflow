#! /usr/bin/env bash
module load clustal
. ~luisdiaz/software/seaview/initialize

INPUT_FOLDER=$1 
RESULTS_FOLDER=$2 
TARGET_FILE=$3
virus_reference=$4

quasispecies_file_string="/-SAMPLE-/haploclique-assembly_0000/quasispecies.fasta"

mkdir -p $RESULTS_FOLDER/trees
# echo "
# 	$TARGET_FILE
# 	$RESULTS_FOLDER/trees
# 	$INPUT_FOLDER$quasispecies_file_string
# 	$virus_reference
# 	3
# "
custom_scripts/compile_sequences.rb $TARGET_FILE $RESULTS_FOLDER/trees $INPUT_FOLDER$quasispecies_file_string $virus_reference 3 #column of target file which groups sequences to make the trees.
						 #Must be >= 3 
for f in `ls $RESULTS_FOLDER/trees/*.fasta`
do
	OUT_DEND=`echo $f|sed 's/fasta/dnd/g'`
	custom_scripts/launch_clustal_for_tree.rb $f $OUT_DEND
	OUT_PDF_DEND=`echo $f|sed 's/fasta/pdf/g'`
	seaview -unrooted -o $OUT_PDF_DEND -plotonly $OUT_DEND
done
