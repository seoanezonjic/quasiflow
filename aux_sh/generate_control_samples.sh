#! /usr/bin/env bash
#SBATCH --ntasks=1
#SBATCH --mem=10gb
#SBATCH --time='7-00:00:00'
#SBATCH --constraint=cal
#SBATCH --error=job.%J.err
#SBATCH --output=job.%J.out

. ~soft_bio_267/programs/x86_64/art/initializes
. ~soft_bio_267/initializes/init_ruby
#PATH="/mnt/home/users/bio_264_uma/luisdiaz/software/art":$PATH
#export PATH
#module load ruby/2.2.2p95

coverage=300000
percentage_affected_regions=20
percentage_nucleotides_mutated=10
recomb_per_recomb_reference=15
distance_between_recs=60
seq_profile='-ss MSv3 -p -na -l 250 -m 300 -s 10'

mkdir -p $raw_files

echo -e "Sample\tReplicate\tMutations\tRecombinations" > $TARGET_FILE
rm $SAMPLES_FILE
for replicate in 1 2 3
do
	sample_id="mut_rec_"$replicate #Mut: NO, Rec: NO
	echo $sample_id >> $SAMPLES_FILE
	echo -e "$sample_id\t$replicate\tmut\trec" >> $TARGET_FILE
#	art_illumina -i $virus_reference $seq_profile -f $coverage -o $raw_files"/"$sample_id"_"

	sample_id="mut_REC_"$replicate #Mut: NO, Rec: YES
	echo $sample_id >> $SAMPLES_FILE
	echo -e "$sample_id\t$replicate\tmut\tREC" >> $TARGET_FILE
#	reference_mutator.rb -S -m $distance_between_recs -r $recomb_per_recomb_reference -s $percentage_affected_regions -i $virus_reference -o $raw_files"/"$sample_id'_' -c $coverage -a "$seq_profile"

	sample_id="MUT_rec_"$replicate #Mut: YES, Rec: NO
	echo $sample_id >> $SAMPLES_FILE
	echo -e "$sample_id\t$replicate\tMUT\trec" >> $TARGET_FILE
#	reference_mutator.rb -R -p $percentage_nucleotides_mutated -s $percentage_affected_regions -i $virus_reference  -o $raw_files"/"$sample_id'_' -c $coverage -a "$seq_profile"

	sample_id="MUT_REC_"$replicate #Mut: YES, Rec: YES
	echo $sample_id >> $SAMPLES_FILE
	echo -e "$sample_id\t$replicate\tMUT\tREC" >> $TARGET_FILE
#	reference_mutator.rb -m $distance_between_recs -r $recomb_per_recomb_reference -p $percentage_nucleotides_mutated -s $percentage_affected_regions -i $virus_reference -o $raw_files"/"$sample_id'_' -c $coverage -a "$seq_profile"
done

#find $raw_files -iname '*.fq' -exec gzip -f -9 {} \;
#find $raw_files -iname '*.fq.gz' -type f -exec bash -c 'mv "$1" "${1/fq.gz/fastq.gz}"' -- {} \;
