#! /usr/bin/env bash
. ~soft_bio_267/initializes/init_report_html
. ~soft_bio_267/initializes/init_R

INPUT_FOLDER=$1 
RESULTS_FOLDER=$2 
TARGET_FILE=$3
sample_refs=$4
STAT_FOLDER=$RESULTS_FOLDER"/statistical_analysis"

mkdir -p $STAT_FOLDER

# Get factors for analysis
head -n 1 $TARGET_FILE |tr "\t" "\n"| tail -n +3 > $RESULTS_FOLDER/experiment_factors 
factors=`tr "\n" "," < $RESULTS_FOLDER/experiment_factors`

# Join all results of each sample in a general table
custom_scripts/merge_results.R -t $TARGET_FILE -f $INPUT_FOLDER -o $RESULTS_FOLDER/all_info_table -s extract_data.rb_0000/results/table_results > $RESULTS_FOLDER/old_new_names
merge_tabular.rb `ls $INPUT_FOLDER/*/report_html_0000/temp/freq_mut_by_position` | custom_scripts/rem_string4tab.rb -i - -H -s 0 -m 1 > $RESULTS_FOLDER/var_freq_by_positionNsample
cat $INPUT_FOLDER/*/extract_data.rb_0000/consensus_*.fasta > $RESULTS_FOLDER/all_consensus.fasta

#####################################################
## SEQUENCE SIMILARITY
#####################################################
. ~soft_bio_267/initializes/init_seaview
module load clustal/2.1
custom_scripts/launch_clustal_for_tree.rb $RESULTS_FOLDER/all_consensus.fasta $RESULTS_FOLDER'/consensus.dnd'
seaview -unrooted -o $RESULTS_FOLDER'/consensus.pdf' -plotonly $RESULTS_FOLDER'/consensus.dnd'

#####################################################
## STATISTICAL ANALYSIS
#####################################################

## Anova
#####################################
PVALOR=0.01
custom_scripts/anova.R -d $RESULTS_FOLDER/all_info_table -p $PVALOR -f $factors -c TsTv_ratio > $STAT_FOLDER/anova_results
custom_scripts/anova.R -d $RESULTS_FOLDER/all_info_table -p $PVALOR -f $factors -c HomoRecs >> $STAT_FOLDER/anova_results
custom_scripts/anova.R -d $RESULTS_FOLDER/all_info_table -p $PVALOR -f $factors -c NonHomoRecs >> $STAT_FOLDER/anova_results
custom_scripts/anova.R -d $RESULTS_FOLDER/all_info_table -p $PVALOR -f $factors -c Rec_freq >> $STAT_FOLDER/anova_results
#custom_scripts/anova.R -d $RESULTS_FOLDER/all_info_table -p $PVALOR -f $factors -c HpMutFreq >> $STAT_FOLDER/anova_results
custom_scripts/anova.R -d $RESULTS_FOLDER/all_info_table -p $PVALOR -f $factors -c SNPs_rel >> $STAT_FOLDER/anova_results

for orf in `head -n 1 $RESULTS_FOLDER/all_info_table |tr "\t" "\n"|grep 'Orf'`; do
	custom_scripts/anova.R -d $RESULTS_FOLDER/all_info_table -p $PVALOR -f $factors -c $orf >> $STAT_FOLDER/anova_results
done

## PCA
#####################################

SUPLEMENTARY_VARS="'"`cat $RESULTS_FOLDER/experiment_factors|sed ':a;N;$!ba;s/\n/,/g'|sed "s/,/','/g"`"'"
orfs=`head -n1 $RESULTS_FOLDER/all_info_table |tr "\t" "\n"|grep 'OrfMutFreq'|sed ':a;N;$!ba;s/\n/,/g'|sed "s/,/', '/g"`

# Using general vars
#------------------------------------
echo "general vars"
echo "-------------"
PCA_variables=`echo -e "
'Rec_freq',
'HomoRecs',
'NonHomoRecs',
'ShannonDivI',
'SampNucDivEntity',
'Haplot_tot',
'SNPs_rel',
'InDels_rel',
'Transits_rel',
'Transvers_rel',
'TsTv_ratio',
'$orfs'" | tr -d [:space:]`
#'VFPKM', Not used with mixed experimental and synthethic data due to synthectic data is pure virus and experimental data contains plant reads
# 'HpMutFreq',

if [ "$sample_refs" != "" ]; then
	sample_refs="-r $sample_refs"
fi
factor_analysis.R -I -d $RESULTS_FOLDER/all_info_table -l "$SUPLEMENTARY_VARS" -c "$PCA_variables" -o $STAT_FOLDER/general_pca $sample_refs > $STAT_FOLDER/PCA_general_results
parse_pca.rb $STAT_FOLDER"/PCA_general_results" > $STAT_FOLDER"/PCA_general_results_parsed"
#mv Investigate.html $STAT_FOLDER/PCA_general_facto_rep.html


# Using total vars
#------------------------------------ 
echo "total vars"
echo "-------------"
PCA_variables="
'Rec_freq',
'ShannonDivI',
'SampNucDivEntity',
'SNPs_rel',
'InDels_rel',
'Transits_rel',
'Transvers_rel',
'TsTv_ratio',
'PopNucDivI',
'SimpsonDivI',
'GinSimpDivI',
'HillQ1DivI',
'HillQ2DivI',
'Haplot_tot',
'`head -n1 $RESULTS_FOLDER/all_info_table |tr "\t" "\n"|grep 'OrfMutFreq'|sed ':a;N;$!ba;s/\n/,/g'|sed "s/,/', '/g"`'"
#'`head -n1 $RESULTS_FOLDER/all_info_table |tr "\t" "\n"|grep '[AGTC]\.[AGTC]_rel'|sed ':a;N;$!ba;s/\n/,/g'|sed "s/,/', '/g"`',
#'HpMutFreq',
factor_analysis.R -I -d $RESULTS_FOLDER/all_info_table -l "$SUPLEMENTARY_VARS" -c "$PCA_variables" -o $STAT_FOLDER/total_pca $sample_refs > $STAT_FOLDER/PCA_total_results
parse_pca.rb $STAT_FOLDER"/PCA_total_results" > $STAT_FOLDER"/PCA_total_results_parsed"
#mv Investigate.html $STAT_FOLDER/PCA_total_facto_rep.html

#####################################################
## MAKE REPORT
#####################################################
current=`pwd`
cd $RESULTS_FOLDER
files=`echo "
	experiment_factors,
	all_info_table,
	var_freq_by_positionNsample,
	old_new_names,
	$STAT_FOLDER/anova_results,
	$STAT_FOLDER/PCA_total_results_parsed,
	$STAT_FOLDER/PCA_general_results_parsed
	" | tr -d [:space:] `

report_html -t $templates_path'/comparative_report.erb' -d $files -o comparative_report
cd $current


