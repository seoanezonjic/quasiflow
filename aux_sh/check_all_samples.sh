#! /usr/bin/env bash
. ~soft_bio_267/initializes/init_autoflow

OUTPUT_FOLDER=$1
SAMPLE_LIST=$2
FLAG=$3
mkdir $OUTPUT_FOLDER
while read p; do
	echo $p
	#flow_logger -e $OUTPUT_FOLDER/$p $FLAG
	flow_logger -e $OUTPUT_FOLDER/$p $FLAG -r all
done < $SAMPLE_LIST
