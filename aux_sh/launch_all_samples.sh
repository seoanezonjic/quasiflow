#! /usr/bin/env bash
#. ~soft_cvi_114/initializes/init_autoflow
. ~soft_bio_267/initializes/init_autoflow

OUTPUT_FOLDER=$1
SAMPLE_LIST=$2
OTHER_VARS=$3
VERBOSE=$4
echo $4
mkdir $OUTPUT_FOLDER
while read p; do
	echo 'Launching '$p
	if [ "$VERBOSE" == "r" ]; then
        	flow_logger -e $OUTPUT_FOLDER/$p -w -l #-p
	else
		AutoFlow -w $CODE_PATH'/templates/workflow_template' -c 8 -s -n 'cal' -t '2-00:00:00' -m '40Gb' -o $OUTPUT_FOLDER/$p -V "\$workflow_sample=$p,$OTHER_VARS" $VERBOSE
		#AutoFlow -w workflow_template -c 50 -s -n 'cal|bigmem' -t '2-00:00:00' -o $OUTPUT_FOLDER/$p -V "\$workflow_sample=$p,$OTHER_VARS" $VERBOSE
	fi
done < $SAMPLE_LIST
