#! /usr/bin/env bash
module load autoflow/last
mkdir varscan_iterative
read_samples=`cat  samples_to_process.lst|tr "\n" ";"`

AutoFlow -w iterative_varscan -c 1 -s -n 'cal|bigmem' -t '7-00:00:00' $1 -o varscan_iterative -V '$samples='$read_samples,'$workflow_path=/mnt/scratch/users/bio_264_uma/luisdiaz/workflow/analysed_samples,$bam_path=bwa_0001/reads.bam'

