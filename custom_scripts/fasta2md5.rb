#! /usr/bin/env ruby
require 'digest'

file = ARGV[0]

name = nil
seq = ''
File.open(file).each do |line|
	line.chomp!
	if line =~ /^>/
		if !name.nil?
			puts Digest::MD5.hexdigest(seq)
			seq = ''
		end
		name = line.gsub('>','')
	else
		seq << line
	end

end
puts Digest::MD5.hexdigest(seq)
