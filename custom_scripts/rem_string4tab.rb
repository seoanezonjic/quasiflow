#! /usr/bin/env ruby

require 'optparse'


#################################################################################################
## INPUT PARSING
#################################################################################################
options = {}

optparse = OptionParser.new do |opts|
    options[:input] = nil
    opts.on( '-i', '--input_file PATH', 'Reference table' ) do |string|
        options[:input] = string
    end

    options[:string] = nil
    opts.on( '-s', '--string STRING', 'String to search in each column' ) do |string|
        options[:string] = string
    end

    options[:header] = false
    opts.on( '-H', '--header', 'Input file has header' ) do
        options[:header] = true
    end

    options[:missmatches] = 0
    opts.on( '-m', '--missmatches INTEGER', 'Number of columns that can contain other value than set in "-s"' ) do |string|
        options[:missmatches] = string.to_i
    end

    # Set a banner, displayed at the top of the help screen.
    opts.banner = "Usage: #{__FILE__} options \n\n"

    # This displays the help screen
    opts.on( '-h', '--help', 'Display this screen' ) do
            puts opts
            exit
    end

end # End opts

# parse options and remove from ARGV
optparse.parse!

#####################################################################################
## MAIN
####################################################################################

if options[:input] == '-'
	input = STDIN
else
	input = File.open(options[:input])
end
# REmove rows with the same value in all positions
count = 0
input.each do |line|
	fields = line.chomp.split("\t")
	if fields.length - options[:missmatches] - fields.count(options[:string]) > 0 || 
		(options[:header] && count == 0)
		puts fields.join("\t")
	end
	count += 1
end