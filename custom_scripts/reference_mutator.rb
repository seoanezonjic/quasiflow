#! /usr/bin/env ruby
require 'optparse'

#################################################################################################
## METHODS
#################################################################################################
def load_fasta(file)
	seqs = []
	name = nil
	seq = ''
	File.open(file).each do |line|
		line.chomp!
		if line =~ /^>/
			if !name.nil?
				seqs << [name, seq.upcase!]
				seq = ''
			end
			name = line
		else
			seq << line
		end
	end
	seqs << [name, seq.upcase!]
	return seqs
end

def do_recombinations(reference_seqs, recombination_per_reference, minimum_distance)
	seqs = []
	border_limit = 10
	reference_seqs.each do |name, seq|
		cuts = get_recombination_coords(seq, border_limit, recombination_per_reference, minimum_distance)
		puts 'dir-dir, rev-rev', cuts.inspect
		new_seq = ''
		while !cuts.empty?
			if new_seq == ''
				low = 0
				upper = cuts.shift
			elsif cuts.length == 1
				low = cuts.shift
				upper = seq.length-1
			else
				low, upper = cuts.shift(2)
			end
			@rec_coords << [upper+1, cuts.first+1, 'd'] if !cuts.empty?
			new_seq << seq[low..upper]
		end
		seqs << [name + '_REC', new_seq]
	end
	return seqs
end

def do_snps(reference_seqs, percentage_mutated_nucleotides)
	seqs = []
	nucs = %w[A C T G]
	reference_seqs.each do |name, seq|
		seq_length = seq.length
		mutated_nucleotides = (percentage_mutated_nucleotides * seq_length.to_f/100).to_i
		mutation_coords = []
		while mutation_coords.length != mutated_nucleotides
			coord = rand(seq_length - 1)
			mutation_coords << coord if !mutation_coords.include?(coord)
		end
		new_seq = seq.clone
		mutation_coords.each do |coord|
			@snp_coords << coord+1
			current = new_seq[coord]
			new_seq[coord] = (nucs - [current.upcase]).sample
		end
		seqs << [name + '_SNP', new_seq]
	end
	return seqs
end

def get_recombination_coords(seq, border_limit, recombination_per_reference, minimum_distance)
	length = seq.length - border_limit
	upper_limit = length - border_limit # We remove border regions
	cuts = [rand(length-1)]
	rec_number = recombination_per_reference*2
	expected_distance = (length-border_limit*2)/rec_number
	puts "Expected_distance: #{expected_distance}"
	if expected_distance - minimum_distance <= 30
		abort("The minimum_distance must be minor than #{expected_distance.to_i-30} with the given parameter")
	end
	while cuts.length < rec_number
		cut = rand(upper_limit-1)
		adyacent_cuts = cuts.map{|c| (c-cut).abs}.count{|diff| diff < minimum_distance}	
		cuts << cut if adyacent_cuts == 0 && cut > border_limit
	end
	cuts.sort!
	return cuts
end


def do_recombination_dir_rev(reference_seqs, recombination_per_reference, minimum_distance)
	seqs = []
	border_limit = 10
	equivalence_table = {
		'A' => 'T',
		'C' => 'G',
		'T' => 'A',
		'G' => 'C',
		'N' => 'N'
	}
	reference_seqs.each do |name, seq|
		cuts = get_recombination_coords(seq, border_limit, recombination_per_reference, minimum_distance)		
		puts 'dir,rev', cuts.inspect
		cuts.unshift(0)
		cuts << seq.length-1
		new_seq = ''
		count = 0
		while cuts.length > 1
			low = cuts.shift
			upper = cuts.first
			cuts[0] = upper + 1
			local_seq = seq[low..upper]
			if count % 2 != 0
				local_seq = local_seq.reverse.split('').map{|nt| equivalence_table[nt]}.join
				@rec_coords << [low+1, upper+1, 'r']
			end
			count += 1
			new_seq << local_seq
		end
		seqs << [name + '_REC_dir_rev', new_seq]
	end
	return seqs
end

#################################################################################################
## INPUT PARSING
#################################################################################################
options = {}

optparse = OptionParser.new do |opts|
    options[:input] = nil
    opts.on( '-i', '--input_file PATH', 'Reference sequence' ) do |string|
        options[:input] = string
    end

    options[:execute] = TRUE
    opts.on( '-E', '--not_execute', 'Do not generate synthetic reads' ) do
        options[:execute] = FALSE
    end

    options[:recombinations] = TRUE
    opts.on( '-R', '--not_recombinations', 'Not generate recombinations' ) do
        options[:recombinations] = FALSE
    end

    options[:snps] = TRUE
    opts.on( '-S', '--not_snps', 'Not generate snps' ) do
        options[:snps] = FALSE
    end

    options[:combine] = FALSE
    opts.on( '-C', '--combine', 'The reference sequences with variations have recombinations AND SNP, otherwise there will be references with SNP and references with recombinations' ) do
        options[:combine] = TRUE
    end


    options[:output] = nil
    opts.on( '-o', '--output_file PATH', 'File for Art' ) do |string|
        options[:output] = string
    end

    options[:percentage_affected_sequences] = 1
    opts.on( '-s', '--percentage_affected_sequences INTEGER', 'Percentage of references that must be recombined forms' ) do |string|
        options[:percentage_affected_sequences] = string.to_f
    end

    options[:percentage_mutated_nucleotides] = 10
    opts.on( '-p', '--snp_percentage INTEGER', 'Percentage of SNP on mutated references' ) do |string|
        options[:percentage_mutated_nucleotides] = string.to_f
    end


    options[:recombination_per_reference] = 1
    opts.on( '-r', '--recombination_per_reference INTEGER', 'Number of cuts in each recombination' ) do |string|
        options[:recombination_per_reference] = string.to_f
    end

    options[:minimum_distance] = 50
    opts.on( '-m', '--minimum_distance INTEGER', 'Minimun distance in nucleotides between two consecutive sequence cuts' ) do |string|
        options[:minimum_distance] = string.to_i
    end

    options[:coverage] = 1000
    opts.on( '-c', '--coverage INTEGER', 'Average coverage to create the synthetic reads' ) do |string|
        options[:coverage] = string.to_i
    end

	options[:art_cmd] = ''
    opts.on( '-a', '--art_cmd STRING', 'art commandline without  -f, -i or -o flags ' ) do |string|
        options[:art_cmd] = string
    end

    # Set a banner, displayed at the top of the help screen.
    opts.banner = "Usage: #{__FILE__} options \n\n"

    # This displays the help screen
    opts.on( '-h', '--help', 'Display this screen' ) do
            puts opts
            exit
    end

end # End opts

# parse options and remove from ARGV
optparse.parse!

#####################################################################################
## MAIN
####################################################################################
# 1% event => 100 references
# 0.01 % event => X references
iterations = (100 / options[:percentage_affected_sequences]) 
if options[:combine] || options[:recombinations] 
	iterations -= 2  # 2 recombined refs: dir & dir vs rev
end
if !options[:combine] && options[:snps]
	iterations -= 1
end	
reference_seqs = load_fasta(options[:input])

recombined_reference = nil
recombined_reference_dir_rev = nil
reference_with_snps = nil
@snp_coords = []
@rec_coords = []
if options[:combine]
		recombined_reference = do_recombinations(reference_seqs, options[:recombination_per_reference], options[:minimum_distance])
		recombined_reference_dir_rev = do_recombination_dir_rev(reference_seqs, options[:recombination_per_reference], options[:minimum_distance])
		recombined_reference = do_snps(recombined_reference, options[:percentage_mutated_nucleotides])
		recombined_reference_dir_rev = do_snps(recombined_reference_dir_rev, options[:percentage_mutated_nucleotides])
else
	if options[:recombinations]
		recombined_reference = do_recombinations(reference_seqs, options[:recombination_per_reference], options[:minimum_distance])
		recombined_reference_dir_rev = do_recombination_dir_rev(reference_seqs, options[:recombination_per_reference], options[:minimum_distance])
	end
	if options[:snps]
		reference_with_snps = do_snps(reference_seqs, options[:percentage_mutated_nucleotides])
	end
end

## SAVE FASTA
File.open(options[:output]+'_rec_seqs', 'w') do |f|
	if options[:recombinations] || options[:combine]
		f.puts recombined_reference.join("\n")
		f.puts recombined_reference_dir_rev.join("\n")
	end
	if options[:snps] && !options[:combine]
		f.puts reference_with_snps
	end
	iterations.to_i.times do |n|
		reference_seqs.each do |name, seq|
			f.puts "#{name}_#{n}\n#{seq}"
		end
	end
end
File.open(options[:output]+'coords_rec', 'w') do |f|
	@rec_coords.each do |rec|
		f.puts rec.join("\t")
	end
end
File.open(options[:output]+'coords_snp', 'w') do |f|
        @snp_coords.sort.each do |snp|
                f.puts snp
        end
end
## Compute coverage for Art
ref_nucleotides = reference_seqs.map{|ref| ref[1].length}.inject(0){|i,j| i+j }
recombined_ref_nucleotides = 0
if options[:recombinations]
	recombined_ref_nucleotides = recombined_reference.map{|ref| ref[1].length}.inject(0){|i,j| i+j }
	recombined_ref_nucleotides += recombined_reference_dir_rev.map{|ref| ref[1].length}.inject(0){|i,j| i+j }
end
coverage = iterations + recombined_ref_nucleotides.to_f/ref_nucleotides
final_coverage = (options[:coverage]/coverage).to_i
puts final_coverage
cmd = "art_illumina -i #{options[:output]}_rec_seqs -o #{options[:output]} -f #{final_coverage} #{options[:art_cmd]} &> art_log" 
puts cmd
system(cmd) if options[:execute]
