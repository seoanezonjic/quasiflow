#! /usr/bin/env ruby

# ALL metrics based in https://pubmed.ncbi.nlm.nih.gov/27060566/

def load_alignment(fasta_alignment, seq_reference_name)
	reference = nil
	all_seqs = []
	names = []

	name = nil
	seq = ''
	File.open(fasta_alignment).each do |line|
		line.chomp!
		if line =~ /^>/
			if !name.nil?
				if name == seq_reference_name
					reference = seq
				else
					names << name
					all_seqs << seq
				end
				seq = ''
			end
			name = line.gsub('>', '')
		else
			seq << line.downcase
		end
	end

	if name == seq_reference_name
		reference = seq
	else
		names << name
		all_seqs << seq
	end
	return all_seqs, reference, names
end

def load_frecuency_table(file)
	table = {}
	File.open(file).each do |line|
		line.chomp!
		fields = line.split("\t")
		freq = fields.last.to_f
		table[fields.first] = freq if freq > 0
	end
	return table
end

def identify_snps(all_seqs, reference, names, seq_reference_name)
	halp_with_snp = {}
	ref_length = reference.length
	all_seqs.each_with_index do |seq, i|
		snp_positions = [] # 1 based snp coordinates
		ref_length.times do |position|
			char_seq = seq[position]
			char_ref = reference[position]
			if char_seq == '-' && char_ref == '-'
				next
			else
				if char_seq.downcase != char_ref.downcase
					snp_positions << position + 1 
				end
			end
		end
		if !snp_positions.empty?
			halp_with_snp[names[i]] = snp_positions
		end
	end
	halp_with_snp[seq_reference_name] = []
	return halp_with_snp
end

def index_by_snp(halp_with_snp)
	snp_assoc_halp = {}
	halp_with_snp.each do |halp, snps|
		snps.each do |snp|
			query = snp_assoc_halp[snp]
			if query.nil?
				snp_assoc_halp[snp] = [halp]
			else
				query << [halp]
			end
		end
	end
	return snp_assoc_halp
end

def get_pairwise_distances(halp_with_snp)
	matrix = []
	halp_with_snp.each do |hp_id1, snps1|
		local_vector = []
		halp_with_snp.each do |hp_id2, snps2|
			local_vector << (snps1 + snps2 - (snps1 & snps2)).length
		end
		matrix << local_vector
	end
	return matrix
end

def get_MutFreqEntity(hapl_number, master_hapl_length, halp_with_snp)
	all_muts = 0
	halp_with_snp.each do |hpl, snps|
		all_muts += snps.length
	end
	index_MutFreqEntity = all_muts.fdiv(hapl_number)/master_hapl_length
	return index_MutFreqEntity
end

def get_ShannonDivI(hapl_number, hapl_frequencies)
	sum = 0
	hapl_frequencies.each do |id, freq|
		sum +=  Math.log(freq) * freq
	end
	return -sum.fdiv(Math.log(hapl_number))
end

def get_SampNucDivEntity(master_hapl_length, hapl_number, genetic_distances)
	all_sum = genetic_distances.flatten.inject(0){|sum, x| sum + x }
	return all_sum.fdiv(master_hapl_length * hapl_number * (hapl_number - 1))
end

def get_PopNucDivI(master_hapl_length, hapl_frequencies, genetic_distances)
	sum = 0
	hpl_ids = hapl_frequencies.keys
	hpl_ids.each_with_index do |id1, i|
		hpl_ids.each_with_index do |id2, j|
			sum += genetic_distances[j][i] * hapl_frequencies[id1] * hapl_frequencies[id2]
		end
	end
	return sum.fdiv(master_hapl_length)
end

def get_SimpsonDivI(hapl_frequencies)
	return hapl_frequencies.values.map{|freq| freq ** 2 }.inject(0){|sum, x| sum + x }
end

def get_Hill(hapl_frequencies, q)
	if q == 1 # 1 causes an indefinition that is approached by limits. See http://onlinelibrary.wiley.com/doi/10.2307/1934352/pdf
		sum = 0
		hapl_frequencies.each do |id, freq|
			sum +=  Math.log(freq) * freq
		end
		return Math.exp(-sum) 
	else
		return (hapl_frequencies.values.map{|freq| freq ** q }.inject(0){|sum, x| sum + x }) ** (1/(1 - q))
	end
end

def get_FuncAttDiv(master_hapl_length, genetic_distances)
	all_sum = genetic_distances.flatten.inject(0){|sum, x| sum + x }
	return all_sum.fdiv(master_hapl_length)
end

###########################################################################
## MAIN
###########################################################################


seq_reference_name = ARGV[0]
fasta_alignment = ARGV[1]
hapl_frequency_file = ARGV[2]

if File.exists?(fasta_alignment)
	all_seqs, reference, names = load_alignment(fasta_alignment, seq_reference_name)
	hapl_frequencies = load_frecuency_table(hapl_frequency_file)
	
	if !reference.nil?
		halp_with_snp = identify_snps(all_seqs, reference, names, seq_reference_name)
		genetic_distances = get_pairwise_distances(halp_with_snp)
		snp_assoc_halp = index_by_snp(halp_with_snp)
		
		# parameters for calculate indexes
		master_hapl_length = reference.count('^-') #Amplicon length, remove gaps
		hapl_number = halp_with_snp.length
		snp_number = snp_assoc_halp.length

		complexity_index = []
		complexity_index << ['Haplot_tot', false, hapl_number] #Mfe
		complexity_index << ['MutFreqEntity', true, get_MutFreqEntity(hapl_number, master_hapl_length, halp_with_snp)] #Mfe
		complexity_index << ['ShannonDivI', false, get_ShannonDivI(hapl_number, hapl_frequencies)] #Hsh
		complexity_index << ['SampNucDivEntity', true, get_SampNucDivEntity(master_hapl_length, hapl_number, genetic_distances)] #Pi_e
		complexity_index << ['PopNucDivI', true, get_PopNucDivI(master_hapl_length, hapl_frequencies, genetic_distances)] #Pi
		simpson = get_SimpsonDivI(hapl_frequencies) #Hsi
		complexity_index << ['SimpsonDivI', false, get_SimpsonDivI(hapl_frequencies)] #Hsi
		complexity_index << ['GinSimpDivI', false, 1 - simpson] #Hgs
		complexity_index << ['HillQ1DivI', false, get_Hill(hapl_frequencies, 1)] #qH
		complexity_index << ['HillQ2DivI', false, get_Hill(hapl_frequencies, 2)] #qH
		complexity_index << ['FuncAttDiv', false, get_FuncAttDiv(master_hapl_length, genetic_distances)] #FAD

		complexity_index.each do |index_name, sci_not, value|
			if sci_not
				puts "#{index_name}\t#{"%e" %value}"
			else
				puts "#{index_name}\t#{value}"
			end	
		end

		#data plots
		###################

		# DATA PREPARATION
		table_data = {}
		halp_with_snp.each do |hpl_id, snps|
			n_snp = snps.length
			query_hapl = hapl_frequencies[hpl_id]
			next if query_hapl.nil?
			query = table_data[n_snp]
			if query.nil?
				table_data[n_snp] = [query_hapl]
			else
				query << query_hapl
			end
		end

		output_folder = 'table_data'
		Dir.mkdir(output_folder) if !File.exists?(output_folder)
		# Montserrat
		File.open(File.join(output_folder, 'montserrat_table.txt'), 'w') do |f|
			f.puts "Mut_n\tFreq"
			table_data.to_a.sort!{|h1,h2| h1.first <=> h2.first}.each do |n_snp, freqs|
				freqs.sort!{|f1,f2| f2 <=> f1}.each do |fr|
					f.puts "#{n_snp}\t#{fr}"
				end
			end
		end

		# Mutations_vs_hpl
		File.open(File.join(output_folder, 'mutations_vs_hpl_table.txt'), 'w') do |f|
			f.puts "Muts\tHpls"
			table_data.sort{|d1, d2| d1.first <=> d2.first}.each do |n_snp, freqs|
				f.puts "#{n_snp}\t#{freqs.length}"
			end
		end

		# Renyi
		File.open(File.join(output_folder, 'renyi_profile.txt'), 'w') do |f|
			f.puts "q\tLnHill"
			5.times do |q|
				f.puts "#{q}\t#{Math.log(get_Hill(hapl_frequencies, q))}"
			end
		end

		# Pairwise distances
		File.open(File.join(output_folder, 'pairwise_distances_profile.txt'), 'w') do |f|
			f.puts "GeneticDist\tPercPairs"
			n = genetic_distances.length
			n_pairs = n * (n-1)
			all_distances = genetic_distances.flatten - [0]
			uniq_distances = all_distances.uniq.sort
			uniq_distances.each do |dist|
				f.puts "#{dist}\t#{all_distances.count(dist).fdiv(n_pairs) * 100}"
			end
		end
	end
end

