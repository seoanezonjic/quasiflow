#! /usr/bin/env ruby
require 'color_converter'

def load_alignment(fasta_alignment, seq_reference_name)
	reference = nil
	all_seqs = []
	freqs = []
	names = []

	name = nil
	seq = ''
	File.open(fasta_alignment).each do |line|
		line.chomp!
		if line =~ /^>/
			if !name.nil?
				if name == seq_reference_name
					reference = seq
				else
					all_seqs << seq
				end
				seq = ''
			end
			name = line.gsub('>', '')
			if name != seq_reference_name
				names << name
				freqs << line.split("_").last.to_f 
			end
		else
			seq << line.downcase
		end
	end

	if name == seq_reference_name
		reference = seq
	else
		all_seqs << seq
	end
	return all_seqs, names, reference, freqs
end

def get_color_code(count)
	#puts count.inspect
	# del rojo puro al amarillo
	colors = []
	color_system = 255
	r=5
	r.times do |i|
	    x = i*color_system/r;
	    colors << [color_system, x ,0]
	end
	 
	# del amarillo al verde
	g=5
	g.times do |i|
	    x=color_system-i*color_system/g; 
	    colors << [x, color_system, 0]
	end
	 
	# verde puro
	#colors << [0, 255, 0]

	color_number  = r + g #red -> high freq, green -> low freq
	max = 8 # Logaritmico, para tylcv a 6
	index = (-Math.log10(count) * color_number / max).to_i
	#puts colors.inspect, index.inspect
	rgb = colors[index]
	rgb = colors.last if rgb.nil?
	#puts "#{colors.length}\t#{count}\t#{-Math.log10(count)}\t#{index}"
	return ColorConverter.hex(rgb[0], rgb[1], rgb[2])
end

def get_network(all_snp)
	nodes_freq = Hash.new(0)
	nodes_type = {}
	edges = []
	all_snp.each do |name, freq, snps|
		nodes_type[name] = 0 #haplotipe
		nodes_freq[name] = freq * 100
		snps.each do |snp|
			nodes_type[snp] = 1 #snp
			nodes_freq[snp] += freq * 100
			edges << [name, snp, freq * 100] 
		end
	end
	nodes = []
	nodes_freq.each do |node, freq|
		nodes << [node, nodes_type[node], freq]
	end
	return edges, nodes
end

def get_array(all_snp)
	array_representation = []
	array_representation << ['haplotipe', 'pos', 'freq']

	count = 1
	all_snp.each do |name, freq, snps|
		snps.each do |snp|
			array_representation << [count, snp, freq]
		end
		count += 1
	end
	return array_representation
end

###########################################################################
## MAIN
###########################################################################


seq_reference_name = ARGV[0]
fasta_alignment = ARGV[1]


all_seqs = []
all_snp = []
names = []
relations = Hash.new(0) # pairs of snp nor haplotipes
if File.exists?(fasta_alignment)

	all_seqs, names, reference, freqs = load_alignment(fasta_alignment, seq_reference_name)

	if !reference.nil?
		ref_length = reference.length
		all_seqs.each_with_index do |seq, i|
			snp_positions = [] # 1 based snp coordinates
			ref_length.times do |position|
				char_seq = seq[position]
				char_ref = reference[position]
				if char_seq == '-' && char_ref == '-'
					next
				else
					if char_seq.downcase != char_ref.downcase
						snp_positions << position + 1 
					end
				end
			end
			if !snp_positions.empty?
				snp_positions.repeated_combination(2).to_a.each do |relation|
					relations[relation] += freqs[i]
				end
				all_snp << ["hp#{i+2}", freqs[i], snp_positions] # +1 to base 1 counts and other +1 to reserve the first index to the master hapl
			end
		end
	end
end
names.unshift(seq_reference_name)

File.open('table_names', 'w') do |f|
	names.each_with_index do |name, i|
		f.puts "#{name}\t#{i+1}"
	end 
end

# SNP network (only snps within same haplotipe are connected)
relations.delete_if{|relation, count| relation.first == relation.last }
File.open("#{seq_reference_name}.dot", 'w') do |file|
	file.puts  'digraph {', 'overlap = false;' #last sentence is for neato
	relations.each do |relation, count|
		color_code = get_color_code(count)
		#file.puts "\"#{relation.first}\" -> \"#{relation.last}\"[dir=none, penwidth=#{'%.12f' % count}]"
		file.puts "\"#{relation.first+1}\" -> \"#{relation.last+1}\"[dir=none, color=\"#{color_code}\", penwidth=2]"
	end
	file.puts  '}'
end

# SNP - Haplotipe bipartite network 
all_relations, nodes = get_network(all_snp)

haplotipe_array = get_array(all_snp)
file = File.open("haplotipe_array", 'w')
n_seqs = 2 + all_seqs.length
file.puts haplotipe_array.shift.join("\t") 
haplotipe_array.each do |hp_ind, pos, freq|
	file.puts "#{n_seqs - hp_ind.to_i}\t#{pos}\t#{freq}"
end
file.close

######################################
## Sparse matrix
######################################
haplotipe_array.shift
hapl = haplotipe_array.map{|record| record[0]}.uniq.sort
hapl_index = {}
hapl.each_with_index do |h, i|
	hapl_index[h] = i
end

pos = haplotipe_array.map{|record| record[1]}.uniq.sort
pos_index = {}
pos.each_with_index do |p, i|
	pos_index[p] = i
end

matrix = Array.new(hapl.length){Array.new(pos.length){0}}
haplotipe_array.each do |hpl, pos, freq|
	matrix[hapl_index[hpl]][pos_index[pos]] = freq
end

File.open('sparse_matrix', 'w') do |f|
	f.puts "Hpl\t#{pos.join("\t")}"
	f.puts "1\t#{Array.new(pos_index.length, 0).join("\t")}" # master haplotype
	matrix.each_with_index do |hpl, i|
		f.puts "#{hapl[i]+1}\t#{hpl.join("\t")}"
	end
end

######################################

file = File.open("edges", 'w')
file.puts ['from', 'to', 'weight'].join(",") 
all_relations.each do |relation|
	file.puts relation.join(',')
end
file.close

file = File.open("nodes", 'w')
file.puts ['id', 'type', 'freq'].join(",") 
nodes.each do |relation|
	file.puts relation.join(",")
end
file.close
