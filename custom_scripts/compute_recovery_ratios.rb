#! /usr/bin/env ruby


require 'optparse'
################################################################################################
## METHODS
################################################################################################
def load_sample_data(file)
    attributes = {}
    header, values = File.open(file).readlines.map{|l| l.split("\t")}
    header.each_with_index do |head, i|
        attributes[head] = values[i].to_f
    end
    return attributes
end

def load_ref_snp(file)
    ref_snp = []
    if File.exists?(file)
        ref_snp = File.open(file).readlines.map{|l| l.to_i}
    end
    return ref_snp
end

def load_test_snp(file)
    return File.open(file).readlines.select{|l| l !~ /^#/}.map{|l| l.split("\t")[1].to_i}
end

def load_ref_rec(file)
    data = []
    if File.exists?(file)
        data = File.open(file).readlines.map{|l| l.chomp.split("\t") }
        data.map!{|f| [f[0].to_i, f[1].to_i, f[2]]}
    end
    return data
end

def load_test_rec(file)
    data = File.open(file).readlines.map{|l| l.chomp.split("\t")}
    data.map!{|f| 
        coords = [f[0].to_i, f[1].to_i].sort
        if f[3] == 'NonHomoRecs'
        #if f[3] == 'Double' # NonHomoRecs
            coords << 'r'
        elsif f[3] == 'HomoRecs'
        #elsif f[3] == 'Single' # HomoRecs
            coords << 'd'  
        end
        coords
    }
    return data
end

def count_common_recombinations(ref_rec, test_rec)
    count = 0
    common_recs = []
    ref_rec.each do |r_start, r_stop, r_type|
        test_rec.each do |t_start, t_stop, t_type|
            if r_type == t_type
                if (r_start-t_start).abs <= 3 && (r_stop-t_stop).abs <=3
                    count += 1
        		    common_recs << [[r_start, r_stop, r_type], [t_start, t_stop, t_type]]
                    break
                end
            end
        end
    end
    return count, common_recs
end
#################################################################################################
## INPUT PARSING
#################################################################################################
options = {}

optparse = OptionParser.new do |opts|
    options[:sample_data] = nil
    opts.on( '-t', '--sample_data_file PATH', 'File with the sample attributes computed by QuasiFlow' ) do |path|
        options[:sample_data] = path
    end

    options[:test_snp_file] = nil
    opts.on( '-s', '--test_snp_file PATH', 'File with SNP coordinates to test' ) do |path|
        options[:test_snp_file] = path
    end

    options[:test_rec_file] = nil
    opts.on( '-r', '--test_rec_file PATH', 'File with recombination coordinates to test' ) do |path|
        options[:test_rec_file] = path
    end

    options[:ref_snp_file] = nil
    opts.on( '-S', '--ref_snp_file PATH', 'File with SNP coordinates to take as reference' ) do |path|
        options[:ref_snp_file] = path
    end

    options[:ref_rec_file] = nil
    opts.on( '-R', '--ref_rec_file PATH', 'File with recombination coordinates to take as reference' ) do |path|
        options[:ref_rec_file] = path
    end

    options[:sample_name] = nil
    opts.on( '-n', '--sample_name STRING', 'Identifier to use as tag whe the ratios are printed' ) do |string|
        options[:sample_name] = string
    end       

    # Set a banner, displayed at the top of the help screen.
    opts.banner = "Usage: #{File.basename(__FILE__)} options \n\n"

    # This displays the help screen
    opts.on( '-h', '--help', 'Display this screen' ) do
            puts opts
            exit
    end

end # End opts

# parse options and remove from ARGV
optparse.parse!
#####################################################################################
## MAIN
####################################################################################

sample_data = load_sample_data(options[:sample_data])
ref_snp = load_ref_snp(options[:ref_snp_file])
test_snp = load_test_snp(options[:test_snp_file])
ref_rec = load_ref_rec(options[:ref_rec_file])
test_rec = load_test_rec(options[:test_rec_file])

ref_rec.select!{|rec| rec.last != 'r'} # REmove reversocomplementary recombinations
ratios = []
ratios << ['RejectedReads', (sample_data['RejectedReads_tot']-sample_data['ReferenceReads_tot'])/2/sample_data['Reads_tot_init']] # #virus reads are considered low quality/contaminants because are filtered by the virus ref. We discount them from the rejected total
ratios << ['ReferenceReads', sample_data['ReferenceReads_tot']/2/sample_data['Reads_tot_init']]
ratios << ['MappedReads', sample_data['mapped_reads']/sample_data['ReferenceReads_tot']]
ref_test_common_recombinations, recs_attributes = count_common_recombinations(ref_rec, test_rec)
test_ref_common_recombinations, recs_attributes = count_common_recombinations(test_rec, ref_rec)
if ref_rec.length > 0
    ratios << ['TrueRECs', ref_test_common_recombinations.fdiv(ref_rec.length)]
    STDERR.puts ['TrueRECs_ALL', (ref_test_common_recombinations + (test_rec.length-ref_test_common_recombinations)).fdiv(ref_rec.length)].inspect
else
    ratios << ['TrueRECs', 0]
end
if  test_rec.length > 0
    ratios << ['FalseRECs', (test_rec.length - test_ref_common_recombinations).fdiv(test_rec.length)]
else
    ratios << ['FalseRECs', 0]
end
common_snp = ref_snp & test_snp
if ref_snp.length > 0
    ratios << ['TrueMUTs', common_snp.length.fdiv(ref_snp.length)]
else
    ratios << ['TrueMUTs', 0]
end
# if test_snp.length > 0
#     ratios << ['FalseSNP', (test_snp.length - common_snp.length).fdiv(test_snp.length)]
# else
#     ratios << ['FalseSNP', 0]
# end
#ratios << ['Haplotypes', sample_data['Haplot_tot']/sample_data['Haplot_raw']]
#final_ref_recs = ref_rec - recs_attributes
#final_test_recs = test_rec - recs_attributes
#STDERR.puts final_ref_recs.sort.inspect, '-', final_test_recs.sort.inspect
ratios.each do |ratio_data|
    ratio_data.unshift(options[:sample_name])
    puts ratio_data.join("\t")
end
