#! /usr/bin/env ruby

clique_file = ARGV[0]

haplotypes = []
haplo_seq = nil
freq = nil
snp = []
File.open(clique_file).each do |line|
	line.chomp!
	next if line.include?('SNV got') || line.include?('parameters')
	if line.include?('=')
		tag, data = line.split('=', 2)
		data = data.strip.gsub(/[,']/,'')
		if tag.include?('snp')
			haplotypes << [snp, freq, haplo_seq] if !haplo_seq.nil?
			vars_string, pos_string = data.scan(/([ACTGN]+)\[(.+)\]/).first
			if vars_string.nil?
				snp = []
			else
				vars = vars_string.split('')
				pos = pos_string.split(' ').map{|p| p.to_i}
				snp = pos.zip(vars)
			end
		elsif tag.include?('frequency')
			freq = data.to_f
		elsif tag.include?('haplotype')
			haplo_seq = data
		end
	end

end

haplotypes << [snp, freq, haplo_seq] if !haplo_seq.nil?
count = 0
haplotypes.each do |vars, freq, seq|
	vars.each do |pos, nt|	
		puts ["hp#{count}" , pos, nt].join("\t")
	end
	count +=1 
end
