#! /usr/bin/env ruby
require 'json'
#require 'math'

#################################################################################
## METHODS
################################################################################

def get_varscan_data(line, processed)
	# Total_variants is equal to SNPs or to Indels in each case. This id due to the dedicated executions to each variant type
	vars = %w{SNPs_tot SNPs_rel InDels_tot Indels_rel Transits_tot Transvers_tot TsTv_ratio Transits_rel Transvers_rel }
	vars.each do | var| 
		processed[var] = 0 if processed[var].nil? #This method can be called several times, so data can be overwrited. This avoids this problem
	end
	check_snp_data = false
	if File.exists?(File.join(line, "results.dump"))
		file = File.open(File.join(line, "results.dump")).read
    	file.gsub!('$VAR1 = ', '')
    	if !file.include?('undef')
	        data = eval(file)
	        if !data['all']['snp_count'].nil?
		        processed['SNPs_tot'] = data['all']['snp_count']
		        processed['SNPs_rel'] = data['all']['snp_count'].to_f/processed['ntMapped2Reference_tot'].to_f if !processed['ntMapped2Reference_tot'].nil? 

		        #processed['Total_variants'] = data['all']['count']
		        #processed['Total_variants_rel'] = data['all']['count'].to_f/processed['ntMapped2Reference_tot'].to_f if !processed['ntMapped2Reference_tot'].nil? 

		        data['all']['snp'].each do |key, val|
		        	key = key.gsub('>', '2')
				    processed[key+'_tot'] = val
			        processed[key+'_rel'] = val.to_f/processed['ntMapped2Reference_tot'].to_f if !processed['ntMapped2Reference_tot'].nil? 
		        end
				check_snp_data = true
	    	elsif !data['all']['indel_count'].nil? 
		        processed['InDels_tot'] = data['all']['indel_count']
		        processed['InDels_rel'] = data['all']['indel_count'].to_f/processed['ntMapped2Reference_tot'].to_f if !processed['ntMapped2Reference_tot'].nil? 
	    	end
	    end
	end


	if File.exists?(File.join(line, "results.tstv")) && check_snp_data
        header = %w{Transits Transvers TsTv_ratio}
    	file = File.open(File.join(line, "results.tstv")).readlines
        file.last.split("\t")[0..2].each_with_index do |var, i|
        	key = header[i]
        	label = key == 'TsTv_ratio' ? 'TsTv_ratio' : key + '_tot' 
    		processed[label] = var
    		processed[key+'_rel'] = var.to_f/processed['ntMapped2Reference_tot'].to_f if !processed['ntMapped2Reference_tot'].nil? && key != 'TsTv_ratio'
        end
	end
	if check_snp_data
		max = 0
		min = 1
		if File.exists?(File.join(line, "validated_vars.vcf"))
			File.open(File.join(line, "validated_vars.vcf")).each do |line|
				next if line =~ /^#/
				fields = line.chomp.split("\t")
				var_attributes = fields[9].split(':')
				ad = var_attributes[5].to_i
				dp = var_attributes[3].to_i
				freq = ad.fdiv(dp)
				max = freq if freq > max
				min = freq if freq < min
			end
		end
		processed['min_snp_freq'] = min
		processed['max_snp_freq'] = max
	end
	return processed
end


# def get_seqtrimnext_data(line, processed)
# 	vars = %w{RawReads_tot RejectedReads_tot ReferenceReads_tot ReferenceReads_perc VFPKM}
# 	vars.each do | var| 
# 		processed[var] = 0
# 	end
# 	if File.exists?(File.join(line, 'output_files', "stats.json"))
# 	        file = File.open(File.join(line, 'output_files', "stats.json")).read
#         	data = JSON.parse(file)
# 		processed['RawReads_tot'] = data['sequences']['count']['input_count'].to_i
# 		if !data['sequences']['count']['rejected'].nil?
# 			processed['RejectedReads_tot'] = data['sequences']['count']['rejected']
# 		end
# 		total_count = 0
# 		data['PluginUserContaminants']['user_contaminant_ids'].each do |id, count|
# 			total_count += count.to_i
# 		end
# 		processed['ReferenceReads_tot'] = total_count
# 		virus_length = File.open(File.join(line,'virus_length')).read.to_i
# 		vir_and_host_reads = data['sequences']['count']['output_seqs_paired'].to_i + data['sequences']['count']['output_seqs'].to_i
# 		processed['VFPKM'] = (total_count/2)/(vir_and_host_reads/1000000.0)/(virus_length/1000) # FPKM taken from http://www.rna-seqblog.com/rpkm-fpkm-and-tpm-clearly-explained
# 		processed['ReferenceReads_perc'] = total_count.to_f/processed['RawReads_tot']*100
# 	end
	
# 	return processed
# end

def get_seqtrimbb_data(line, processed)
        vars = %w{RawReads_tot RejectedReads_tot ReferenceReads_tot ReferenceReads_perc VFPKM HostReads_tot}
        vars.each do | var|
                processed[var] = 0
        end
        plugin_log = File.join(line, 'output_files', "plugins_logs", 'virus_ref_user_filter_filtering_stats_by_length')
        if File.exists?(plugin_log)
        	File.open(plugin_log).each do |l|
        		if l =~ /^Output:/
        			reads = l.split(':')[1].strip.split(' ').first
					processed['ReferenceReads_tot'] = reads.to_i
					break
				end
        	end
		end
        if File.exists?(File.join(line, 'output_files', "stats.json"))
			file = File.open(File.join(line, 'output_files', "stats.json")).read
			data = JSON.parse(file)
			processed['RawReads_tot'] = data['sequences']['input_count']
			processed['RejectedReads_tot'] = data['sequences']['rejected']
			#processed['ReferenceReads_tot'] = data['plugin_user_filter']['filtered_sequences_count'] # this amount of reads ar not included in output count and the short reads are not removed
			virus_length = File.open(File.join(line,'virus_length')).read.to_i
			vir_and_host_reads = data['sequences']['output_count'] + processed['ReferenceReads_tot']
			processed['HostReads_tot'] = data['sequences']['output_count']
			processed['VFPKM'] = (processed['ReferenceReads_tot']/2)/(vir_and_host_reads/1000000.0)/(virus_length/1000) # FPKM taken from http://www.rna-seqblog.com/rpkm-fpkm-and-tpm-clearly-explained
			processed['ReferenceReads_perc'] = processed['ReferenceReads_tot'].to_f/processed['RawReads_tot']*100
        end

        return processed
end

def get_bwa_data(line, processed)
	processed['ntMapped2Reference_tot'] = 0
	processed['mapping_coverage'] = 0
	processed['mapped_reads'] = 0
	if File.exists?(File.join(line, "nucleotide_count"))
		total_count = 0
		File.open(File.join(line, "nucleotide_count")).each do |line|
			line.chomp!
			fields = line.split
			total_count += fields.first.to_i
		end
		processed['ntMapped2Reference_tot'] = total_count
	end
	if File.exists?(File.join(line, 'qualimap_report', 'genome_results.txt'))
		File.open(File.join(line, 'qualimap_report', 'genome_results.txt')).each do |line|
			line.chomp!
			if line.include?('mean coverageData')
				processed['mapping_coverage'] = line.split('=').last.gsub(',','').to_f
			elsif line.include?('number of mapped reads')
				mapped = line.split('=').last.gsub(',','').strip
				processed['mapped_reads'] = mapped.split(' ').first.to_i
			elsif line.include?('number of supplementary alignments')
				mapped = line.split('=').last.gsub(',','').strip
				processed['supp_alignments'] = mapped.split(' ').first.to_i
				processed['mapped_reads'] = processed['mapped_reads'] - processed['supp_alignments'] # Qualimap counts supplementary alignments as real reads
			end			
		end

	end
	return processed
end

def get_clustalo(line, processed)
	#vars = %w{Nucleotide_diversity Num_haplotypes Shannon_index} # Old
	vars = %w{SampNucDivEntity Haplot_tot Haplot_raw ShannonDivI SampNucDivEntity PopNucDivI SimpsonDivI GinSimpDivI HillQ1DivI HillQ2DivI FuncAttDiv}
	vars.each do | var| 
		processed[var] = 0
	end
	if File.exists?(File.join(line, "results"))
		File.open(File.join(line, "results")).each do |line|
			line.chomp!
			fields = line.split("\t")
			processed[fields.first] = fields.last
		end
	end
	initial_alignment = File.join(line, 'quasispecies_alignment.aln')
	if File.exists?(initial_alignment)
		processed['Haplot_raw'] = File.open(initial_alignment).read.count('>')
	end

	if File.exists?(File.join(line, 'table_data'))
		processed['ComplexityIndexFolder'] = File.join(line, 'table_data')
	end
	return processed
end

def get_virema_data(line, processed)
	vars = %w{Total_reads_analysed_by_Virema Indels_by_Virema Ambigous_recombination_events_by_Virema HomoRecs NonHomoRecs HomoRecs_rel NonHomoRecs_rel RecombinationData RecNumber Rec_freq}
	vars.each do | var| 
		processed[var] = 0
	end
	stats_path = File.join(line, "stats")
	if File.exists?(stats_path)
        File.open(stats_path).each do |line|
			line.chomp!
			if line =~ /Total of (\d*) reads have been analysed:/
				processed['Total_reads_analysed_by_Virema'] = $1
            elsif /(\d*) were MicroIndels below a threshold of less than or equal to (\d*) nucleotides./ =~ line
                    processed['Indels_by_Virema'] = $1
            elsif /(\d*) events were Unknown or Ambiguous Recombination Events\./ =~ line
                    processed['Ambigous_recombination_events_by_Virema'] = $1
			end
		end
    end
    rec_stats_path = File.join(line, "results", "Recombinations_stats")
    if File.exists?(rec_stats_path)
        File.open(rec_stats_path).each do |line|
			fields = line.chomp.split("\t")
			rec_type = fields.first
			rec_number = fields.last.to_i
			processed['RecNumber'] += rec_number
			processed[rec_type] = rec_number
			freq = rec_number.fdiv(processed['Total_reads_analysed_by_Virema'].to_i)
			processed[rec_type + '_rel'] = freq
			processed['Rec_freq'] += freq

        end
        processed['RecombinationData'] = File.join(line, 'results', 'Recombinations_data')
    end

	return processed
end

def get_haploclique_data(line, processed)
	processed['HpMutFreq'] = 0
	if File.exists?(File.join(line, "statistics_input.txt"))
		sum_freqs = 0
		length = 0
		mut_freqs_file = File.open('mut_frequency_haploclique', 'w')
		File.open(File.join(line, "statistics_input.txt")).each do |line|
			line.chomp!
			fields = line.split("\t")
			if length == 0
				length += 1
				next 
			end
			freq_mut = 1 - fields[1..4].map{|frq| frq.to_f}.max
			mut_freqs_file.puts "#{fields[0]}\t#{freq_mut}"
			sum_freqs += freq_mut
			length += 1
		end
		processed['HpMutFreq']= sum_freqs/length
		mut_freqs_file.close
	end	
	return processed
end

def get_virvarseq_data(line, processed, iteration)
	pattern = File.join(line, 'results', 'mixture_model', '*.codon') #Automatic quality thresold
	paths = Dir.glob(pattern)
	if paths.empty?
		pattern = File.join(line, 'results', 'codon_table', '*.codon') #Fixed quality thresold
		paths = Dir.glob(pattern)		
	end
	path = paths.first
	if !path.nil? && File.exists?(path)
		freqs = {}
		File.open(path).each do |line|
			line.chomp!
			fields = line.split("\t")
			if fields[4] == fields[5]
				query = freqs[fields[1]]
				if query.nil?
					freqs[fields[1]] = fields[20].to_f
				else
					freqs[fields[1]] += fields[20].to_f
				end
			end
		end
		sum_freqs = 0
		freqs.each do |position, fr|
			sum_freqs += (1 - fr)
		end
		processed['OrfMutFreq'+iteration]= sum_freqs/freqs.length if freqs.length > 0 
	end
	return processed
end

def get_fastqc_data(line, processed, iteration)
	path = File.join(line, "read_data")
	if File.exists?(path)
		count = 0
		header = []
		values =[]
		File.open(path).each do |line|
			line.chomp!
			if count == 0
				header = line.split("\t")
			elsif count == 1
				values = line.split("\t")
			end
			count += 1
		end
		tag=""
		if iteration == "0000"
			tag = "_init"
		elsif iteration == "0001"
			tag = "_final"
		end
		header.each_with_index do |head, i|
			processed[head+tag] = values[i]
		end
	end
	return processed
end

#################################################################################
## MAIN
#################################################################################
processed = {}
executions_file = File.open(ARGV[0]).readlines.each do |line|
	line.chomp!
	execution_name = File.basename(line)
	execution_name =~ /(.+)_(\d{4})/
	program = $1
	iteration = $2
	if program == 'varscan'
		processed = get_varscan_data(line, processed)
	elsif program == 'seqtrimnext'
		processed = get_seqtrimnext_data(line, processed)
	elsif program == 'seqtrimbb'
		processed = get_seqtrimbb_data(line, processed)
	elsif program =='ViReMa.py'
		processed = get_virema_data(line, processed)
	elsif program =='haploclique-assembly'
        processed = get_haploclique_data(line, processed)
	elsif program =='clique-snv'
       # processed = get_clique_snv_data(line, processed)
	elsif program =='bwa'
	    processed = get_bwa_data(line, processed)
	elsif program =='clustalo'
        processed = get_clustalo(line, processed)
	elsif program =='codon_table.pl'
        processed = get_virvarseq_data(line, processed, iteration)
	elsif program =='fastqc'
        processed = get_fastqc_data(line, processed, iteration)
	end
end

snps_tot = processed['SNPs_tot']
indels_tot = processed['InDels_tot']
processed['vars_tot'] = snps_tot + indels_tot if !snps_tot.nil? && !indels_tot.nil?

snps_rel = processed['SNPs_rel']
indels_rel = processed['InDels_rel']
processed['vars_rel'] = snps_rel + indels_rel if !snps_rel.nil? && !indels_rel.nil?


puts processed.keys.join("\t")
puts processed.values.join("\t")

