#! /usr/bin/env ruby
require 'optparse'
require 'benchmark'

FROM  = 0
TO = 1
COUNT = 2
REC_TYPE = 3
ORIGIN_SEQ = 4
DEST_SEQ = 5

#################################################################################################
## METHODS
#################################################################################################
def load_virema_file(file, circular, exclude_coords)
	rec_events = {}
	recombination_type = nil
	origin_seq = nil
	destination_seq = nil
	File.open(file).each do |line|
		line.chomp!
		if line == '@EndofLibrary'
			next
		elsif line =~ /@NewLibrary: (\w*)_RevStrand_to_(\w*)_RevStrand/
			#recombination_type = :Single
			recombination_type = :HomoRecs
			origin_seq = $1
			destination_seq = $2
			#recombination_type = "RevvsRev"
		elsif line =~ /@NewLibrary: (\w*)_to_(\w*)_RevStrand/
			#recombination_type = :Double
			recombination_type = :NonHomoRecs
			origin_seq = $1
			destination_seq = $2
			#recombination_type = "DirvsRev"
		elsif line =~ /@NewLibrary: (\w*)_RevStrand_to_(\w*)/
			#recombination_type = :Double
			recombination_type = :NonHomoRecs
			origin_seq = $1
			destination_seq = $2
			#recombination_type = "DirvsRev"
		elsif line =~ /@NewLibrary: (\w*)_to_(\w*)/
			#recombination_type = :Single
			recombination_type = :HomoRecs
			origin_seq = $1
			destination_seq = $2
			#recombination_type = "DirvsDir"
		else
			results = line.scan(/(\d*)_to_(\d*)_#_(\d*)/)
			if !results.empty?
				results.each do |from, to, count|
					rec = [from.to_i, to.to_i]
					rev = false
					if rec.last - rec.first < 0 # Convert all cooirdinates to only one sense
						rec.reverse!
						rev = true 
					end
					next if circular && !(exclude_coords-rec).empty?
					rec.concat([count.to_i, recombination_type, rev])
					query_ori = rec_events[origin_seq]
					if query_ori.nil?
						rec_events[origin_seq] = {destination_seq => [rec]}
					else
						query_dest = query_ori[destination_seq]
						if query_dest.nil?
							query_dest[destination_seq] = [rec]
						else
							query_dest << rec
						end
					end
				end
			end
		end
	end
	return rec_events
end

def load_virema_bed(file, circular, exclude_coords)
	rec_events = {}
	File.open(file).each do |line|
		line.chomp!
		next if line.include?('track')
		fields = line.split("\t")
		origin_seq = fields[0]
		coordinates = [fields[1].to_i, fields[2].to_i].sort
		#destination_seq = fields[3]
		#destination_seq = origin_seq if destination_seq == 'NAMES_TBD'
		destination_seq = origin_seq # Virema 0.20
		rec_result = fields[3] # Virema 0.20
		count = fields[4].to_i
		next if count == 1 # Discard events supported by only a single read
		sense = fields[5]
		#if sense == '+'
		if rec_result == 'Deletion' # Virema 0.20
			recombination_type = 'HomoRecs'
			rev = false
		elsif rec_result == 'Duplication' # Virema 0.20
			recombination_type = 'NonHomoRecs'
			rev = true
		end

		#STDERR.puts "#{exclude_coords.inspect}-#{coordinates.inspect}"
		next if circular && (exclude_coords-coordinates).empty? # Exclude circularization point
		coordinates.concat([count, recombination_type, rev])
		query_ori = rec_events[origin_seq]
		if query_ori.nil?
			rec_events[origin_seq] = {destination_seq => [coordinates]}
		else
			query_dest = query_ori[destination_seq]
			if query_dest.nil?
				query_dest[destination_seq] = [coordinates]
			else
				query_dest << coordinates
			end
		end
	end
	return rec_events
end

def aggregate_recs(rec_events)
	results = []
	rec_events.each do |ori_seq, data|
		data.each do |dest_seq, recs|
			recs.sort!{|r1, r2| r1[FROM] <=> r2[FROM]}
			while !recs.empty?
				rec1 = recs.shift
				cluster = [rec1]
				index2delete = []
				#STDERR.puts '------------'
				recs.each_with_index do |rec2, i|
					similar, too_far = similar_rec?(rec1, rec2)
					if similar
						index2delete << i
						cluster << rec2
					end	
					break if too_far
				end
				index2delete.reverse_each{|i| recs.delete_at(i)}
				STDERR.puts cluster.inspect
				results << merge_rec_records(cluster, ori_seq, dest_seq)
			end
		end
	end
	return results
end

def format_recs(rec_events)
	results = []
	rec_events.each do |ori_seq, data|
		data.each do |dest_seq, recs|
			recs.sort!{|r1, r2| r1[FROM] <=> r2[FROM]}
			recs.each do |rec|
				results << [rec[FROM], rec[TO], rec[COUNT], rec[REC_TYPE], ori_seq, dest_seq]
			end
		end
	end
	return results
end

def similar_rec?(rec1, rec2)
	similar = false
	too_far = false
	if rec1[REC_TYPE] == rec2[REC_TYPE]
		ff = rec1[FROM]-rec2[FROM] # Diference FROM coordinates
		tt = rec1[TO]-rec2[TO] # Diference TO coordinates
		#	STDERR.puts "#{rec1[FROM]}\t#{rec1[TO]}\t#{rec2[FROM]}\t#{rec2[TO]}"
		#	STDERR.puts "#{ff}\t#{tt}"
		if tt.abs <= 1 && ff.abs <= 1 # If diference is +-1 nt, the rec event is the same
			similar = true
		elsif ff < -10 # Events are sorted by FROM coordinate, so when diff is negative, the two FROM coordinates are too far and no makes sense so fllow with the comparisons
			too_far = true
		end
	end
	return similar, too_far
end

def merge_rec_records(cluster, ori_seq, dest_seq)
	from = cluster.map{|c| c[FROM]}.min
	to = cluster.map{|c| c[TO]}.max
	count = cluster.map{|c| c[COUNT]}.inject(0){|sum, ct| sum +ct}
	base_rec = cluster.first
	final_rec = [from, to, count, base_rec[REC_TYPE], ori_seq, dest_seq]
	return final_rec
end
#################################################################################################
## INPUT PARSING
#################################################################################################
options = {}

optparse = OptionParser.new do |opts|
        options[:input] = nil
        opts.on( '-i', '--input_file FILE', 'Virema results file' ) do |input|
            options[:input] = input
        end

        options[:bed] = false
        opts.on( '-b', '--bed', 'Input file is bed format' ) do 
        	options[:bed] = true
        end

        options[:output] = nil
        opts.on( '-o', '--ouput_folder PATH', 'Path to ouput folder which is saved filtered virema results files' ) do |output|
            options[:output] = output
        end

        options[:circular] = false
        opts.on( '-c', '--circular', 'Set if virus sequence is non linear' ) do
            options[:circular] = true
        end

        options[:seq_names] = false
        opts.on( '-s', '--seq_names', 'Output sequence names in recombination data' ) do
            options[:seq_names] = true
        end

        options[:aggregate_recs] = false
        opts.on( '-a', '--aggregate_recs', 'Merge close recombinations' ) do
            options[:aggregate_recs] = true
        end

        options[:exclude_coords] = []
        opts.on( '-e', '--exclude_coords STRING', 'Filter this coords "start:end"' ) do |exclude_coords|
            options[:exclude_coords] = exclude_coords.split(':').map{|c| c.to_i}.sort
        end

        options[:total_reads] = nil
        opts.on( '-t', '--total_reads INTEGER', 'Set the total reads used in virema analysis' ) do |total_reads|
            options[:total_reads] = total_reads.to_i
        end

        options[:thresold_probability] = 0
        opts.on( '-p', '--:thresold_probability FLOAT', 'Set the minimum probability to take in account a recombination' ) do |thresold_probability|
            options[:thresold_probability] = thresold_probability.to_f
        end
        opts.banner = "Usage: table_header.rb -t tabulated_file \n\n"

        # This displays the help screen
        opts.on( '-h', '--help', 'Display this screen' ) do
                puts opts
                exit
        end

end # End opts

# parse options and remove from ARGV
optparse.parse!

###########################################################################################################
### MAIN
###########################################################################################################
STDERR.puts options.inspect
if options[:bed]
	rec_events = load_virema_bed(options[:input], options[:circular], options[:exclude_coords])
else
	rec_events = load_virema_file(options[:input], options[:circular], options[:exclude_coords])
end
if options[:aggregate_recs]
	rec_events = aggregate_recs(rec_events)
else
	rec_events = format_recs(rec_events)
end
stats_h = Hash.new(0)
File.open(File.join(options[:output], "Recombinations_data"), 'w') do |f|
	rec_events.sort.each do |from, to, count, recombination_type, origin_seq, destination_seq|
		#STDERR.puts [from, to, count, recombination_type, origin_seq, destination_seq].inspect
		if count > 1
			prob = count.to_f / options[:total_reads]
			if prob >= options[:thresold_probability] #count > 10 
				#f.puts "#{from}\t#{to}\t#{prob}\t#{recombination_type}"
				stats_h[recombination_type] += count
				if options[:seq_names]
					f.puts "#{origin_seq}\t#{from}\t#{destination_seq}\t#{to}\t#{count}\t#{recombination_type}"
				else
					f.puts "#{from}\t#{to}\t#{count}\t#{recombination_type}"
				end
			end
		end
	end
end
stats = File.open(File.join(options[:output], "Recombinations_stats"), 'w')
stats_h.each do |recombination_type, total_count|
	stats.puts "#{recombination_type}\t#{total_count}"
end
stats.close
