#! /usr/bin/env ruby

dnd_file = ARGV[0]
table_name = ARGV[1]

dnd_string = File.open(dnd_file).read

id2name = []
File.open(table_name).each do |line|
	line.chomp!
	id2name << line.split("\t")
end
id2name.each do |name, id|
	dnd_string.gsub!(name, id)
end
puts dnd_string