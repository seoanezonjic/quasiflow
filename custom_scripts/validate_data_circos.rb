#! /usr/bin/env ruby

#require 'math'
require 'optparse'

#################################################################################################
## INPUT PARSING
#################################################################################################
options = {}

optparse = OptionParser.new do |opts|
        options[:data] = nil
        opts.on( '-d', '--data_file FILE', 'Circos input data file' ) do |data|
            options[:data] = data
        end

        options[:system_coordinates] = 0
        opts.on( '-c', '--system_coordinates INTEGERS', 'System coordinates used in inputa data' ) do |data|
            options[:system_coordinates] = data.to_i
        end

        options[:magnitude] = 'linear'
        opts.on( '-m', '--magnitude STRING', 'Data scale' ) do |data|
            options[:magnitude] = data
        end

        options[:value_for_zero] = 0
        opts.on( '-v', '--value_for_zero FLOAT', 'Dummie value for log(0)' ) do |val|
            options[:value_for_zero] = val.to_f
        end

        opts.banner = "Usage: table_header.rb -t tabulated_file \n\n"

        # This displays the help screen
        opts.on( '-h', '--help', 'Display this screen' ) do
                puts opts
                exit
        end

end # End opts

# parse options and remove from ARGV
optparse.parse!


File.open(options[:data]).each do |line|
	line.chomp!
	fields = line.split(" ")
	if fields[1] =~ /[a-zA-Z]/
		next
	else
		if options[:magnitude] == 'log'
			value = Math.log10(fields[3].to_f) 
		elsif options[:magnitude] == '-log'
			if fields[3].to_f > 0.0
				value = Math.log10(fields[3].to_f) * -1
			else
				value = options[:value_for_zero]
			end
		elsif options[:magnitude] == 'linear'
			value = fields[3]
		end
		puts "#{fields[0]} #{fields[1].to_i + options[:system_coordinates]} #{fields[2].to_i + options[:system_coordinates]} #{value}"
	end

end
