#! /usr/bin/env ruby
pca_path = ARGV[0]

data = {}
dim = nil
state = nil
dim_data = nil
empty_dim = false
File.open(pca_path).each do |line|
	line.chomp!
	if line.include?('null device') || line.include?('$call')
		break
	elsif line.include?('named list()')
		empty_dim = true
	elsif line =~ /\$Dim\.(\d)$/
		if !dim_data.nil? && !empty_dim
			data[dim] = dim_data
		end
		empty_dim = false
		dim = $1
		dim_data = {}
	elsif line =~ /\$quanti/
		state = 'DimQuanti'
		dim_data['Variables'] = []
	elsif line =~ /\$quali/
                state = 'DimQuali'
                dim_data['Factors'] = []
        elsif line =~ /\$category/
                state = 'DimCat'
                dim_data['Categories'] = []
	elsif line.empty? || 
		(line.include?('correlation') && line.include?('p.value')) || 
		(line.include?('R2') && line.include?('p.value')) || 
		(line.include?('Estimate') && line.include?('p.value')) ||
		line.include?('"class"') ||
		line.include?('"condes"') 
		next
	elsif state == 'DimQuanti'
		dim_data['Variables'] << line.split(' ')
	elsif state == 'DimQuali'
		dim_data['Factors'] << line.split(' ')
	elsif state == 'DimCat'
		fields = line.split(' ')
		fields[0] = fields[0].split('=').last
		dim_data['Categories'] << fields
	end
end
data[dim] = dim_data if !empty_dim
data.each do |dim, dim_data|
	puts "#{dim}\tPCA dimension #{dim}\tcolspan\tcolspan"
	dim_data.each do |type, significant_data|
		puts "#{dim}\t<center><b>#{type}</b></center>\tcolspan\tcolspan"
		puts "#{dim}\t<i>Name</i>\t<i>Correlation coef</i>\t<i>p-valor</i>" if type == 'Variables'
		puts "#{dim}\t<i>Name</i>\t<i>R2</i>\t<i>p-valor</i>" if type == 'Factors'
		puts "#{dim}\t<i>Name</i>\t<i>Estimate</i>\t<i>p-valor</i>" if type == 'Categories'
		significant_data.each do |name, value, p_valor|
			puts "#{dim}\t#{name}\t#{value}\t#{p_valor}"
		end
	end
end

