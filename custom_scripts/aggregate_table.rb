#! /usr/bin/env ruby


require 'optparse'
################################################################################################
## METHODS
################################################################################################
def load_table(path)
	table = []
	header = nil
	count = 0
	File.open(path).each do |line|
		line.chomp!
		fields = line.split("\t")
		if count == 0
			header = fields
		else
			table << fields
		end
		count += 1
	end
	return table, header

end

def get_index(header, names)
	indexes = []
	names.each do |name|
		indexes << header.index(name)
	end
	return indexes
end

#################################################################################################
## INPUT PARSING
#################################################################################################
options = {}

optparse = OptionParser.new do |opts|
        options[:input] = nil
        opts.on( '-i', '--input_file PATH', 'File to process' ) do |string|
            options[:input] = string
        end

        options[:key_cols] = []
        opts.on( '-c', '--key_cols STRING', ' Key column names for merge rows: name1,name2...' ) do |string|
            options[:key_cols] = string.split(',')
        end

        options[:aggregate_cols] = []
        opts.on( '-g', '--aggregate_cols STRING', 'Column value names to perform the aggregate operation: name1,name2...' ) do |string|
            options[:aggregate_cols] = string.split(',')
        end

        options[:operation] = 'c'
        opts.on( '-o', '--aggregate_operation STRING', 'Aggreagate operation, c fon concatenation' ) do |string|
            options[:operation] = string.split(',')
        end

        # Set a banner, displayed at the top of the help screen.
        opts.banner = "Usage: #{__FILE__} options \n\n"

        # This displays the help screen
        opts.on( '-h', '--help', 'Display this screen' ) do
                puts opts
                exit
        end

end # End opts

# parse options and remove from ARGV
optparse.parse!

#####################################################################################
## MAIN
####################################################################################
table, header = load_table(options[:input])
key_indexes = get_index(header, options[:key_cols])
aggregate_indexes = get_index(header, options[:aggregate_cols])

groups = {}
table.each do |row|
	keys = []
	key_indexes.each do |index|
		keys << row[index]
	end

	aggregates = []
	aggregate_indexes.each do |index|
		aggregates << row[index]
	end	

	new_key = keys.join('_')
	query = groups[new_key]
	if query.nil?
		groups[new_key] = aggregates.map{|ag| [ag]}
	else
		query.each_with_index do |ag, i|
			ag << aggregates[i]
		end
	end
end

groups.each do |key, aggregates|
	row = [key]
	aggregates.each do |ag|
		if options[:operation] == 'c'
			row << ag.join(',')
		end
	end
	puts row.join("\t")
end