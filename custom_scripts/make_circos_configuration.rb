#! /usr/bin/env ruby

require 'fileutils'
require 'optparse'

#################################################################################################
## INPUT PARSING
#################################################################################################
options = {}

optparse = OptionParser.new do |opts|
        options[:karyotype_file] = nil
        opts.on( '-k', '--karyotype_file FILE', 'Input tabulated file' ) do |karyotype_file|
            options[:karyotype_file] = karyotype_file
        end

        options[:prefix_label] = ''
        opts.on( '-p', '--prefix_label STRING', 'Prefix to use with with labels' ) do |prefix|
            options[:prefix_label] = prefix
        end

        options[:output_folder] = Dir.pwd
        opts.on( '-o', '--output_folder PATH', 'Path to folder which circos files must be saved' ) do |output_folder|
            options[:output_folder] = output_folder
        end

        options[:system_coords] = 0
        opts.on( '-c', '--system_coords INTEGER', 'Base of coordinates used in karyotype_file') do |system_coords|
            options[:system_coords] = system_coords.to_i
        end

		options[:magnitude] = [1, 1]
        opts.on( '-m', '--magnitude STRING', 'Units:multiplier' ) do |mag|
            options[:magnitude] = mag.split(':').map{|i| i.to_i}
        end

		options[:track_length] = 0.06
        opts.on( '-l', '--track_length STRING', 'Length for each data track 0.2,0.4...' ) do |length|
            options[:track_length] = length.split(',').map{|i| i.to_f}
        end

        options[:histograms] = []
        opts.on( '-t', '--histograms PATHS', 'Paths to data, PATH1;PATH2...' ) do |hist|
            options[:histograms] = hist.split(',')
        end

        options[:visualization] = []
        opts.on( '-v', '--visualization_track STRING', 'h for histogram, m for heatmap, l for links, s for highlights E.g hmhm' ) do |vis|
            options[:visualization] = vis.split('')
        end

        options[:heatmap_limits] = nil
        opts.on( '-s', '--heatmap_limits STRING', 'Lower and upper limit for heatmps. Format lower,upper : 0,100' ) do |hl|
            options[:heatmap_limits] = hl.split(',')
        end


        # Set a banner, displayed at the top of the help screen.
        opts.banner = "Usage: table_header.rb -t tabulated_file \n\n"

        # This displays the help screen
        opts.on( '-h', '--help', 'Display this screen' ) do
                puts opts
                exit
        end

end # End opts

# parse options and remove from ARGV
optparse.parse!

#####################################################################################
## MAIN
####################################################################################

if options[:karyotype_file].nil? || !File.exists?(options[:karyotype_file])
	Process.exit
end

FileUtils.mkdir('data') if !Dir.exists?('data')
FileUtils.mkdir('etc') if !Dir.exists?('etc')

paths = {}
paths['data_folder'] = File.join(options[:output_folder], 'data')
paths['etc_folder'] = File.join(options[:output_folder], 'etc')
paths['templates'] = File.join(File.dirname(__FILE__), 'circos_conf_files')

paths['housekeeping'] = File.join(paths['etc_folder'], 'housekeeping.conf')
original_housekeeping = File.join(File.dirname(%x[which circos]), '..', 'etc', 'housekeeping.conf')
housekeeping_file = File.open(original_housekeeping).read
housekeeping_file.gsub!(/max_points_per_track = \d+/, 'max_points_per_track = 50000')
File.open(paths['housekeeping'], 'w') {|f| f.write(housekeeping_file) }

## GENERATE KARYOTYPE FILE
sequences= {}

File.open(options[:karyotype_file]).each do |line|
	line.chomp!
	fields = line.split("\t")
	name = fields.shift + options[:prefix_label]
	sequences[name] = [fields.first, fields.last].map{|coord| coord.to_i - options[:system_coords]}
end

paths['karyotype_path'] = File.join(paths['data_folder'], options[:karyotype_file]+ '_kar')
karyotype = File.open(paths['karyotype_path'], 'w')
count = 1
sequences.each do |sequence_name, coords|
	karyotype.puts "chr - #{sequence_name} #{sequence_name} #{coords.first} #{coords.last} chr#{count}" # the chr field is used to indicate taht color has de sequence
	count += 1
end
karyotype.close

## GENERATE IDEOGRAM FILE
ideogram_file_template = File.open(File.join(paths['templates'], 'ideogram.conf')).read
paths['ideogram'] = File.join(paths['etc_folder'], 'ideogram.conf')
File.open(paths['ideogram'], 'w') {|f| f.write(ideogram_file_template) }

## GENERATE TICKS FILE
ticks_file_template = File.open(File.join(paths['templates'], 'ticks.conf')).read
ticks_file_template.gsub!('multiplier = mult', "multiplier = #{options[:magnitude].last}")
paths['ticks'] = File.join(paths['etc_folder'], 'ticks.conf')
File.open(paths['ticks'], 'w') {|f| f.write(ticks_file_template) }


## GENERATE CONF FILE
conf_file_template = File.open(File.join(paths['templates'], 'circos.conf')).read
conf_file_template.gsub!("chromosomes_units = units", "chromosomes_units = #{options[:magnitude].first}") #magnitude of units to redeuce de number length
conf_file_template.gsub!("karyotype = path", "karyotype = #{paths['karyotype_path']}") # karyotype file whith main axis coords
conf_file_template.gsub!("<<include path_ideogram>>", "<<include #{paths['ideogram']}>>") #ideogram file configures main axis style
conf_file_template.gsub!("<<include path_ticks>>", "<<include #{paths['ticks']}>>") #ticks file configures the scale style of main axis
plot_section = ""
if options[:histograms].length > 0
	plot_section << "<plots>\n"
	if options[:track_length].class != Array
		track_length = Array.new(options[:histograms].length, options[:track_length])
	else
		track_length = options[:track_length]
	end
	track_separation = 0.02
	histogram_template = '
<plot>
min = 0
type = visualization_type
file = DATA_PATH
r1   = r1_lengthr
r0   = r0_lengthr
fill_color = vdgrey
extend_bin = no
<backgrounds>
	# Show the backgrounds only for ideograms that have data
	<background>
	color =  BACKGROUND_COLOR #vvlgrey
	</background>

</backgrounds>
</plot>
'
	last_r = nil
	options[:histograms].each_with_index do |data_path, i|
		if options[:visualization][i] == 'h' || options[:visualization][i] == 'm'
			if i == 0 
				r1 = 1 - track_separation 
			else
				r1 = last_r - track_separation
			end
			r0 = r1 - track_length[i]
			last_r = r0
			data_file_path = File.join(paths['data_folder'], File.basename(data_path))
			FileUtils.cp(data_path, data_file_path)
			custom_track = histogram_template.gsub('DATA_PATH', data_file_path)
			if options[:visualization][i] == 'h'
				visualization_type = 'histogram'
			elsif options[:visualization][i] == 'm'
				#visualization_type = "heatmap\ncolor  = spectral-11-div\n"
				visualization_type = "heatmap\ncolor  = ylorrd-5-seq-rev\n"
				visualization_type << "min*=#{options[:heatmap_limits].first}\nmax=#{options[:heatmap_limits].last}\n" if options[:heatmap_limits].class == Array
			end
			custom_track.gsub!('visualization_type', visualization_type)
			custom_track.gsub!('r1_length', r1.to_s)
			custom_track.gsub!('r0_length', r0.to_s)
			if i % 2 == 0
				custom_track.gsub!('BACKGROUND_COLOR', '240,240,240')
			else
				custom_track.gsub!('BACKGROUND_COLOR', '227,227,227')
			end
			plot_section << custom_track
		end
	end
    plot_section << "</plots>\n"
	plot_section << "\n<links>"
	link_template= '
<link>
file          = DATA_PATH
radius        = radius_lengthr
bezier_radius = 0.01r
thickness     = 1
</link>
'
	options[:histograms].each_with_index do |data_path, i|
		next if options[:visualization][i] != 'l'
	        data_file_path = File.join(paths['data_folder'], File.basename(data_path))
	        FileUtils.cp(data_path, data_file_path)
                custom_track = link_template.gsub('DATA_PATH', data_file_path)
		if !last_r.nil?
			last_r = last_r - 0.02
		else
			last_r = 1
		end
        	custom_track.gsub!('radius_length', last_r.to_s)
		plot_section << custom_track
	end
	plot_section << "</links>"
        plot_section << "\n\n<highlights>"

        options[:histograms].each_with_index do |data_path, i|
                next if options[:visualization][i] != 's'
                data_file_path = File.join(paths['data_folder'], File.basename(data_path))
                FileUtils.cp(data_path, data_file_path)
		high= "\n<highlight>\nfile=" +  data_file_path
		high << "\nstroke_thickness = 1\nstroke_color = black\n</highlight>\n"
                plot_section << high
        end
        plot_section << "\n</highlights>"
end

conf_file_template.gsub!("plot_section", plot_section)
paths['conf'] = File.join(paths['etc_folder'], 'circos.conf')
File.open(paths['conf'], 'w') {|f| f.write(conf_file_template) }


