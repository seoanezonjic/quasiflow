#!/usr/bin/env ruby

cmd="time clustalo -i #{ARGV[0]}  -o /dev/null --guidetree-out=#{ARGV[1]} #{ARGV[2]} --force -v -v -v"

begin
#	STDOUT.sync = true
#  IO.popen(cmd) do |command|
#	command.sync = true
#    command.each do |line|
#      puts line
#      if line.index('Guide-tree computation done') || line.index("Guide-tree computation (mBed) done")
#	puts "Stopping"
#	command.close
#      end
#    end
#  end
        IO.popen(cmd) {|command|
          command.sync = true
          command.each do |line|
	      puts line if line =~ /option|calculation|mBed/
	      if line.index('Guide-tree computation done') || line.index("Guide-tree computation (mBed) done")
	        puts "Stopping"
	        command.close
	      end

	  end
        }

rescue IOError => e
  puts "Stopped"
end
