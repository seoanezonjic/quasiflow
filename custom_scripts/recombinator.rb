#! /usr/bin/env ruby

####################################################
## METHODS
####################################################

def load_fastq(file)
	seqs = []
	seq = []
	count = 1
	File.open(file).each do |line|
		line.chomp!
		seq << line
		if count == 4
			seqs << seq
			seq = []
			count = 0
		end
		count +=1
	end
	return seqs

end

def generate_pairs(recombinations, n_reads)
	pairs = []
	while pairs.length < recombinations
		r1 = rand(n_reads-1)
		r2 = rand(n_reads-1)
		pairs << [r1, r2] if r1 != r2
	end
	return pairs
end

def get_cut_point(read)
	cut = 0
	border_limit = 20 #nt
	read_length = read[1].length
	final_limit = read_length - border_limit - 1
	while cut <= border_limit || cut >= final_limit
		cut = rand(read_length)
	end
	return cut
end

def get_rec_reads(cut_point, r1, r2)
	start = 0..cut_point -1
	final = cut_point..r1[1].length-1
	new_seq_r1, new_seq_r2 = get_seq(start, final, r1[1], r2[1])
	new_qual_r1, new_qual_r2 = get_seq(start, final, r1[3], r2[3])
	r1[1] = new_seq_r1
	r2[1] = new_seq_r2
	r1[3] = new_qual_r1
	r2[3] = new_qual_r2
end

def get_seq(start, final, s1, s2)
	new_s1 = ''
	new_s2= ''	
	new_s1 = s1[start] + s2[final]
	new_s2 = s2[start] + s1[final]
	return new_s1, new_s2
end

####################################################
## MAIN
####################################################

input_file, output_file, perc_recombinations = ARGV
perc_recombinations = perc_recombinations.to_f / 2

seqs = load_fastq(input_file)
recombinations = (seqs.length * perc_recombinations/100).to_i
pairs = generate_pairs(recombinations, seqs.length)

pairs.each do |read1, read2|
	r1 = seqs[read1]
	r2 = seqs[read2]
	cut_point = get_cut_point(r1)
	get_rec_reads(cut_point, r1, r2)
end

new_file = File.open(output_file, 'w')
seqs.each do |seq|
	new_file.puts seq.join("\n")
end
new_file.close
