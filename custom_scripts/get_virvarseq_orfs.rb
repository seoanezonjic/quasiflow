#! /usr/bin/env ruby

def load_gff(file)
	features = {}
	File.open(ARGV[0]).each do |line|
		line.chomp!
		next if line =~ /^#/ || line.empty?
		fields = line.split("\t")
		attributes = {}
		fields[8].split(';').map{|at| at.split('=')}.each do |key, val|
			val.gsub!(/:\d+..\d+/, '') if key == 'ID'
			attributes[key] = val
		end
		attributes['start'] = fields[3].to_i
		attributes['stop'] = fields[4].to_i
		attributes['strand'] = fields[6]
		attributes['feat_type'] = fields[2]
		attributes['seqid'] = fields[0]
		features[attributes['ID']] = attributes
	end
	return features
end

def merge_genes(orfs)
	features.each do |id, attributes|

	end
end

def get_virvarseq_orfs(features)
	virvarseq_orfs = []
	features.each do |id, attributes|
		next if attributes['feat_type'] != 'CDS'
		start = attributes['start']
		stop = attributes['stop']
		len = (stop - start)/3
		if attributes['strand'] == '+' 
			strand_tag = 'dir' 
		else 
			strand_tag = 'rev'
			reg_length = features[attributes['seqid']]['stop']
			start = reg_length - stop + 1
		end
		virvarseq_orfs << [start, len, strand_tag]
	end
	return virvarseq_orfs
end

############################################################
## MAIN
############################################################
gff_path = ARGV[0]
merge_gene = ARGV[1]
orfs, features = load_gff(ARGV[0])
orfs = merge_genes(orfs) if !merge_gene.nil?
virvarseq_orfs = get_virvarseq_orfs(orfs)
STDOUT.puts virvarseq_orfs.map{|orf| orf.join('l')}.join(';')
