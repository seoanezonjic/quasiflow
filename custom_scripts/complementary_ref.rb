#! /usr/bin/env ruby

def rev_comp(seq, nucs)
        seq.reverse!
        seq.upcase!
        seq = seq.split('').map{|nuc|
	       nucs[nuc]
        }.join('')
	return seq
end

fasta = ''
header = nil 
seq =''

nucs = {
	'A' => 'T',
	'T' => 'A',
	'C' => 'G',
	'G' => 'C',
	'N' => 'N'

}
File.open(ARGV[0]).each do |line|
	line.chomp!
	if line =~ /^>/ 
		if !header.nil?
			seq=rev_comp(seq, nucs)
			fasta << ">#{header}\n#{seq}"
		end
		header = line
	else
		seq << line
	end
end
seq=rev_comp(seq, nucs)
fasta << "#{header}\n#{seq}"

puts fasta
