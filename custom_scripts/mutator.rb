#! /usr/bin/env ruby

input, output, mutation_num = ARGV
mutation_num = mutation_num.to_i

mutated_file = File.open(output, 'w')
nucs = ['A', 'C', 'T', 'G']

count = 0
File.open(input).each do |line|
	line.chomp!
	if count == 1
		read_length = line.length - 1
		rand(mutation_num).times do 
			line[rand(read_length)] = nucs[rand(3)]
		end
	end
	mutated_file.puts line
	count += 1
	count = 0 if count == 4
end

mutated_file.close
