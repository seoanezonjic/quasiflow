#! /usr/bin/env ruby

#############################################################################
### METHODS 
#############################################################################
def load_target(target_file, grouping_column)
	target = {}

	count = 0
	File.open(target_file).each do |line|
		line.chomp!
		if line.empty? || count == 0
			count += 1
			next 
		end
		fields = line.split("\t")
		group = fields.delete_at(grouping_column)
		
		query = target[group]
		if query.nil?
			target[group] = [fields]
		else
			target[group] << fields
		end
	end
	return target
end
#############################################################################
### MAIN
#############################################################################
target_file = ARGV[0]

output = ARGV[1]

input_path = ARGV[2]

fasta_ref = ARGV[3]

grouping_column = ARGV[4].to_i - 1 if !ARGV[4].nil?

if !grouping_column.nil? && target_file.downcase != "false"
	target = load_target(target_file, grouping_column)
else
	target = {'seqs' => [["sample"]]}
end


consensus = File.open(fasta_ref).read
consensus.gsub!(/>[^\n]*/,'')
consensus.gsub!("\n",'')

target.each do |group, info|
	group_path = File.join(output, group + '.fasta')
	fasta_group = File.open(group_path, 'w')

	info.each do |sample|
		original_fasta = input_path.gsub('-SAMPLE-', sample.first)
		dir_results = File.dirname(original_fasta)

		count = 1

		coords = {}
		coords_file = File.join(dir_results, 'alignment.prior')
		File.open(coords_file).each do |line|
			line.chomp!
			fields = line.split(" ")
			coords[fields.first] = [fields[5], fields[6]].map{|coord| coord.to_i - 1}
		end
		name = nil
		File.open(original_fasta).each do |line|
			line.chomp!
			if line =~ /^>/				
				line =~ /(Clique_\w*-dir_\d*)/
				name = $1
				line = '>' + sample[2..sample.length - 1].join("_")+"_rep_#{sample[1]}_qsp_#{count}" if !grouping_column.nil?
				count += 1
			else
				current_coords = coords[name]
				length_line = line.length
				seq_5_prime = ""
				seq_3_prime = ""
				seq_5_prime = consensus[0..current_coords.first-1] if current_coords.first > 0
				seq_3_prime = consensus[current_coords.last+1..consensus.length-1] if (consensus.length-1) - current_coords.last > 0 
				line = seq_5_prime + line + seq_3_prime
				# if line.length != consensus.length
				# 	puts name+'-'+sample.first, '-' *10
				# 	puts consensus.length - line.length
				# 	puts "diff 5 - 3 #{current_coords.first-1} - #{(consensus.length) - current_coords.last}"
				# 	puts current_coords.inspect
				# 	puts "5 add 0 - #{current_coords.first - 1}"
				# 	puts "O seq length #{length_line}"
				# 	puts "3 add #{current_coords.last} - #{consensus.length - 1}"
				# end
			end
			fasta_group.puts line
		end
	end
	
	fasta_group.close

end
