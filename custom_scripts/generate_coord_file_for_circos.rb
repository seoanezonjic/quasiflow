#! /usr/bin/env ruby

virus_length = ARGV[0].to_i
name = ARGV[1]
orfs = ARGV[2].split(";")

initial_r = 1.1
final_orfs = []
orfs.each do |orf|
	start, aa_length, strand = orf.split('l')
	nucl_length = aa_length.to_i * 3
	if strand == 'dir'
		start_orf = start.to_i
		end_orf = (start_orf + nucl_length) - 1
	elsif strand == 'rev'
		end_orf = (virus_length - start.to_i) + 1
		start_orf = (end_orf - nucl_length) + 1
	end
	final_orfs << [start_orf, end_orf,]
end

final_orfs.sort!{|o1, o2| o1.first <=> o2.first }



positions = [
	[1.1, 1.11],
	[1.11, 1.12]
]
count = 0
final_orfs.each do |start, stop|
	local_position = count % 2
	highlight = "#{name} #{start} #{stop} fill_color=chr#{count},r0=#{positions[local_position].first}r,r1=#{positions[local_position].last}r"
    puts highlight
	count += 1
end

