#! /usr/bin/env ruby

virus_length = ARGV[0].to_i
start, aa_orf_length, strand = ARGV[1].split('l')
start = start.to_i
var_file = ARGV[2]
min_freq = ARGV[3].to_f
min_coverage = ARGV[4].to_i

stats = {}
last_position = nil
File.open(var_file).each do |line|
	line.chomp!
	position, aa_ref, aa_var, coverage, freq = line.split("\t")
	if (position == last_position && aa_ref != aa_var) || last_position.nil?
		freq = freq.to_f
		coverage = coverage.to_i
		if coverage >= min_coverage && freq >= min_freq
			if stats[position].nil?
				stats[position] = freq
			else
				stats[position] = stats[position] + freq
			end
		end
	end
	last_position = position
end

stats.each do |position, freq|
	position = position.to_i
	if strand == 'dir'
		position = start + 3 * (position -1)
		position_end = position + 2
	elsif strand == 'rev'
		orf_end = (virus_length - start) - 1
		position_end = (orf_end - 3 * (position -1)) + 1
		position = position_end - 2
	end
	puts "#{position}\t#{position_end}\t#{freq}"
end
