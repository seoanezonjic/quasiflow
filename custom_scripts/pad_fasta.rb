#! /usr/bin/env ruby

fasta=''
File.open(ARGV[0]).each do |line|
	line.chomp!
	if line =~ /^>/ && fasta != ''
		fasta.chomp!
		fasta << 'a' * ARGV[1].to_i
	end
	fasta << line+"\n"
end
fasta.chomp!
fasta << 'a' * ARGV[1].to_i

puts fasta
