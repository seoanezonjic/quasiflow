#! /usr/bin/env bash
#$1 => bam file $2 reference fasta file

OUTPUT=$3
filter_bam=$OUTPUT'/filtered.bam'
rm -rf $OUTPUT
mkdir $OUTPUT
ALL_READS=`samtools idxstats $1 | awk '{s+=$3} END {print s}'`
ln -s $1 $filter_bam

THRESOLD=5000000
BIG_RANGE=1000000
LOW_RANGE=50000 #100000
#BIG_FRACTION=`bc <<< "scale=10; $BIG_RANGE/$MAPPING_READS"`
#LOW_FRACTION=`bc <<< "scale=10; $LOW_RANGE/$MAPPING_READS"`
FRACTION=0
SUM=$LOW_RANGE
rm  $OUTPUT'/log'
while [ `echo "$FRACTION<=1"|bc` == 1  ]; do
	FRACTION=`bc <<< "scale=10; $SUM/$ALL_READS"`
	samtools view -s $FRACTION -b $filter_bam  > $OUTPUT'/fraction.bam' 2>>$OUTPUT'/log'
	samtools index $OUTPUT'/fraction.bam' 
	samtools mpileup -BQ 26  -q 30 -d10000000 -f $2 $OUTPUT'/fraction.bam' > $OUTPUT'/Data.mpileup' 2>> $OUTPUT'/log'
	varscan mpileup2snp $OUTPUT'/Data.mpileup' --min-coverage 1000 --min-reads2 10 --min-var-freq 0.0001 --min-avg-qual 26 --p-value 99e-2 --output-vcf > $OUTPUT'/validated_vars.vcf' 2>>$OUTPUT'/log'
	SNPS=`grep -v -c '#' $OUTPUT'/validated_vars.vcf' `
	MAPPING_READS=`samtools idxstats $OUTPUT'/fraction.bam' | awk '{s+=$3} END {print s}'`
	MAPPING_READS=` bc <<< "$MAPPING_READS/2" `
	MAPPING_NUCLEOTIDES=`awk '{SUM += $4} END {print SUM}' $OUTPUT'/Data.mpileup'`
	echo -e "$SNPS\t$MAPPING_READS" >> $OUTPUT'/illumina_data'
	echo -e "$SNPS\t$MAPPING_NUCLEOTIDES" >> $OUTPUT'/illumina_data_nt'
	EXTRACT_FRACTION=""
	if [ `echo "$MAPPING_READS>=$THRESOLD"|bc` == 1   ]
	then
		EXTRACT_FRACTION=$BIG_RANGE
	else
		EXTRACT_FRACTION=$LOW_RANGE
	fi
	#echo "Reads:$MAPPING_READS BF:$BIG_FRACTION LF:$LOW_FRACTION EF:$EXTRACT_FRACTION F:$FRACTION"
	SUM=`bc <<< "$SUM+$EXTRACT_FRACTION"`
done
