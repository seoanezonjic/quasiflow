#! /usr/bin/env ruby
table = []

last_length = 0
files = 0
row_length = 0
ARGV.each do |file_name|
	count = 0
	add_new_rows = []
	File.open(file_name).each do |line|
		line.chomp!
		fields = line.split("\t")
		last_length = fields.length
		if files == 0
			table << fields
		else
			row = table[count]
			if !row.nil?
				row.concat(fields)
			else
				add_new_rows << Array.new(row_length).concat(fields)
			end
		end
		count += 1
	end
	row_length += last_length
	table.each do |line|
		diff = row_length - line.length
		if diff > 0
			line.concat(Array.new(diff))
		end
	end
	table.concat(add_new_rows)
	files +=1 
end

puts ARGV.map{|p| [p, nil]}.join("\t")
table.each do |line|
	puts line.join("\t")
end
