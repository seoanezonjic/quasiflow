#! /usr/bin/env bash
#SBATCH --cpus=16
#SBATCH --mem=50gb
#SBATCH --time=5-00:00:00
#SBATCH --constraint=cal
#SBATCH --error=job.%J.err
#SBATCH --output=job.%J.out
hostname

module load samtools/1.3.1
module load vcftools/0.1.11
module load varscan/2.3.6
./check_snp.sh /mnt/scratch/users/bio_264_uma/luisdiaz/TYLCV_MI/analysed_samples/Col0_7_2/bwa_0001/reads.bam virus.fasta MI_Col0_7_2&
./check_snp.sh /mnt/scratch/users/bio_264_uma/luisdiaz/TYLCV_MI/analysed_samples/Col0_7_4/bwa_0001/reads.bam virus.fasta MI_Col0_7_4&
./check_snp.sh /mnt/scratch/users/bio_264_uma/luisdiaz/TYLCV_MI/analysed_samples/Col0_7_5/bwa_0001/reads.bam virus.fasta MI_Col0_7_5&
./check_snp.sh /mnt/scratch/users/bio_264_uma/luisdiaz/TYLCV_MI/analysed_samples/REV11_7_1/bwa_0001/reads.bam virus.fasta MI_Rev11_7_1&
./check_snp.sh /mnt/scratch/users/bio_264_uma/luisdiaz/TYLCV_MI/analysed_samples/REV11_7_2/bwa_0001/reads.bam virus.fasta MI_Rev11_7_2&
./check_snp.sh /mnt/scratch/users/bio_264_uma/luisdiaz/TYLCV_MI/analysed_samples/REV11_7_3/bwa_0001/reads.bam virus.fasta MI_Rev11_7_3&
./check_snp.sh /mnt/scratch/users/bio_264_uma/luisdiaz/TYLCV_MI/analysed_samples/REV12_7_1/bwa_0001/reads.bam virus.fasta MI_Rev12_7_1&
./check_snp.sh /mnt/scratch/users/bio_264_uma/luisdiaz/TYLCV_MI/analysed_samples/REV12_7_2/bwa_0001/reads.bam virus.fasta MI_Rev12_7_2&
./check_snp.sh /mnt/scratch/users/bio_264_uma/luisdiaz/TYLCV_MI/analysed_samples/REV12_7_3/bwa_0001/reads.bam virus.fasta MI_Rev12_7_3&

./check_snp.sh /mnt/scratch/users/bio_264_uma/luisdiaz/TYLCV_HI/analysed_samples/Col0_7_2/bwa_0001/reads.bam virus.fasta HI_Col0_7_2&
./check_snp.sh /mnt/scratch/users/bio_264_uma/luisdiaz/TYLCV_HI/analysed_samples/REV11_7_3/bwa_0001/reads.bam virus.fasta HI_Rev11_7_3&
./check_snp.sh /mnt/scratch/users/bio_264_uma/luisdiaz/TYLCV_HI/analysed_samples/REV12_7_1//bwa_0001/reads.bam virus.fasta HI_Rev12_7_1&

#./check_snp.sh /mnt/scratch/users/bio_264_uma/luisdiaz/TYLCV_HI/analysed_samples/PolK_7_1/bwa_0001/reads.bam virus.fasta HI_PolK_7_1&
wait
