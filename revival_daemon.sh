#! /usr/bin/env bash
framework_dir=`dirname $0`
export CODE_PATH=$(readlink -f $framework_dir )
export PATH=$CODE_PATH'/aux_sh:'$PATH
export PATH=$CODE_PATH'/custom_scripts:'$PATH
export templates_path=$CODE_PATH/templates

source $1
# Generate folders by default
mkdir $general_path $raw_files $RESULTS_FOLDER
#-----------------------------
OTHER_VARS="\$custom_scripts=$CODE_PATH/custom_scripts,\$report_template=$templates_path/report_template.erb,\$virus_reference=$virus_reference,\$consensus_start=$consensus_start,\$consensus_end=$consensus_end,\$circular="$circular",\$rec_prob=$rec_prob,\$orfs=$orfs,\$raw_files=$raw_files,\$min_quality_read=$min_nt_qual,\$group1=$group1,\$group2=$group2"

#GENERATE CONTROL SAMPLES
# CHECK IF WE CAN FIX THE SEED WITH THE -rs PARAM IN ART AND RERUN TEST
if [ "$2" == "g" ] ; then
        echo "Generating synthetic samples"
        export virus_reference raw_files SAMPLES_FILE virus_reference TARGET_FILE
        #sbatch $CODE_PATH/generate_control_samples.sh
        $CODE_PATH/aux_sh/generate_control_samples.sh
fi

#COMPUTE RATIOS
if [ "$2" == "c" ] ; then
        echo "Compute recovery ratios"
        . ~soft_bio_267/initializes/init_R
        echo -e "sample\tMetric\tRate" > $OUTPUT_FOLDER'/ratios'
        while read id; do
                sample=`echo -e $id | sed 's/_[0-9]//g'`
                compute_recovery_ratios.rb \
                -t $OUTPUT_FOLDER'/'$id'/extract_data.rb_0000/results/table_results' -s $OUTPUT_FOLDER'/'$id'/varscan_0000/validated_vars.vcf' \
                -r $OUTPUT_FOLDER'/'$id'/ViReMa.py_0000/results/Recombinations_data' -S $raw_files'/'$id'_coords_snp' \
                -R $raw_files'/'$id'_coords_rec' -n $sample >> $OUTPUT_FOLDER'/ratios'
        done < $SAMPLES_FILE
        histogram_means.R -d $OUTPUT_FOLDER'/ratios' -b 'sample' -c 'Rate' -x 'Metric' -o $OUTPUT_FOLDER'/ratios'
        cp $OUTPUT_FOLDER'/ratios.pdf' $RESULTS_FOLDER
fi


#CHECK SAMPLES
if [ "$2" == "0" ] ; then
	echo "Cheking workflows stage 1"
	check_all_samples.sh $OUTPUT_FOLDER $SAMPLES_FILE "$3"
fi

#STAGE 1 (workflow)
if [ "$2" == "1" ] ; then
	echo "Launching stage 1"
        echo $3
	launch_all_samples.sh $OUTPUT_FOLDER $SAMPLES_FILE "$OTHER_VARS" "$3"
fi

if [ "$2" == "1b" ] ; then
        echo "Copy files"
	res_folder=$RESULTS_FOLDER'/sample_results'
	var_folder=$RESULTS_FOLDER'/variant_results'
	consensus_folder=$RESULTS_FOLDER'/consensus_results'
	orf_folder=$RESULTS_FOLDER'/variant_results/orf'
	mkdir -p $res_folder $orf_folder $consensus_folder
        echo $SAMPLES_FILE
	while read p; do
                echo $p
		sample_folder=$RESULTS_FOLDER'/sample_results/'$p
		mkdir -p $sample_folder
        	cp $OUTPUT_FOLDER/$p/circos_0000/circos.png $sample_folder
                cp $OUTPUT_FOLDER/$p/report_html_0000/Report.html $res_folder'/'$p'.html'
        	cp $OUTPUT_FOLDER/$p/extract_data.rb_0000/network_with_highlighted_nodes.pdf $res_folder'/'$p'_network.pdf'
                cp $OUTPUT_FOLDER/$p/extract_data.rb_0000/consensus_*.fasta $consensus_folder
                cp $OUTPUT_FOLDER/$p/varscan_0000/validated_vars.vcf $var_folder'/snp_'$p'.vcf'
                cp $OUTPUT_FOLDER/$p/varscan_0001/validated_vars.vcf $var_folder'/indel_'$p'.vcf'
		for orf in $OUTPUT_FOLDER/$p/codon_table.pl_*; do
			codon_file=`ls $orf/formated_data_*`
			file_name=`basename $codon_file`
			cp $codon_file $orf_folder/$p'_'$file_name
		done
	done < $SAMPLES_FILE
fi

#STAGE 2 (Sample comparison)
if [ "$2" == "2" ] ; then
	echo "Launching stage 2"
	compare_all_samples.sh $OUTPUT_FOLDER $RESULTS_FOLDER $TARGET_FILE $REFS
	#sbatch $CODE_PATH'/aux_sh/make_trees.sh $OUTPUT_FOLDER $RESULTS_FOLDER $TARGET_FILE $virus_reference 
fi
